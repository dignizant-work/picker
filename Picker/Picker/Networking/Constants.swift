//
//  Constants.swift
//  NetworkingLayer
//
//  Created by Vavisa - iMac 2 on 4/8/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct K {
    struct ProductionServer {
//        static let baseURL = "http://192.168.1.13:8000/api"
        
//        static let baseURL = "http://picker.vavisa-kw.com/api" // live
        
        static let baseURL = "http://picker.dignizantapps.com/api"

        static let privacy_policy = "/privacy_policy "
    }
    
    struct APIParameterKey {
        static let password = "password"
        static let email = "email"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
    case authentication = "Basic YXBpQEVuZ2luZWVyczpBUElARW5naW5lZXJzXzEyMzY1NA=="
}


//MARK: Global

//let oneSignalAppID = "253bff52-506e-458f-b6ab-c9a478409bb4"  //--- old
let oneSignalAppID = "51675af9-dc92-4e78-95cb-2bfd8ab2b087"
//Your App ID: 51675af9-dc92-4e78-95cb-2bfd8ab2b087

                                                                                                                  
