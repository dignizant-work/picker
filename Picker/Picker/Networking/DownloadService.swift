//
//  DownloadService.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 3/25/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

class DownloadService {
    
    var downloadsSession: URLSession!
    var activeDownloads: [URL: Download] = [:]
    
    func startDownload(_ picked: PickedStruct) {
        let download = Download.init(picked: picked)
        guard let url = URL(string: picked.url) else {return}
        download.task = downloadsSession.downloadTask(with:url)
        download.task?.resume()
        download.isDownloading = true
        activeDownloads[url] = download
    }

}
