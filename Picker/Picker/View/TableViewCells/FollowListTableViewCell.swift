//
//  FollowListTableViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/13/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SDWebImage

protocol FollowListTableViewCellDelegate: class{
    func acceptBtnPressed(_ targetUserId: Int,cell:FollowListTableViewCell)
    func followBtnPressed(_ targetUserId: Int,cell:FollowListTableViewCell)
}

class FollowListTableViewCell: UITableViewCell {
    
    
    weak var cellDelegate: FollowListTableViewCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var follower = Follower()
    
    func configure(follower: Follower){
        self.follower = follower
        
        usernameLabel.text = follower.username
        if let profileImageUrl = follower.profile_image{
            let url = URL(string:profileImageUrl)
//            profileImage.kf.setImage(with: url)
            profileImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
        }
        
        if let isAccepted = follower.is_accepted{
            if isAccepted{
                print("isAccepted")
                self.acceptBtn.layer.borderColor = AppColors.shared.red.cgColor
                self.acceptBtn.setTitleColor(AppColors.shared.red, for: .normal)
                self.acceptBtn.setTitle("Unaccept".localized(), for: .normal)
            }else{
                print("NOT Accepted")
                self.acceptBtn.layer.borderColor = AppColors.shared.blue.cgColor
                self.acceptBtn.setTitleColor(AppColors.shared.blue, for: .normal)
                self.acceptBtn.setTitle("Accept".localized(), for: .normal)
            }
        }
        else{
            self.acceptBtn.isUserInteractionEnabled = false
            self.acceptBtn.alpha = 0
        }
        
        
        if follower.is_follow{
            print("ISFOLLOW")
            self.followBtn.backgroundColor = AppColors.shared.red
            self.followBtn.setTitle("Unfollow".localized(), for: .normal)
        }else{
            print("NOT FOLLOW")
            self.followBtn.backgroundColor = AppColors.shared.blue
            self.followBtn.setTitle("Follow".localized(), for: .normal)
        }
        
    }
    
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-SemiBold", size: 17)
        return label
    }()
    
    lazy var followBtn:FillButton = {
        let btn = FillButton()
        btn.titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        btn.addTargetClosure(closure: { (_) in
            self.cellDelegate?.followBtnPressed(self.follower.id,cell: self)

        })
        return btn
    }()
    
    lazy var acceptBtn:LineButton = {
        let btn = LineButton()
        btn.titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        btn.addTargetClosure(closure: { (_) in
            self.cellDelegate?.acceptBtnPressed(self.follower.id, cell: self)
        })
        return btn
    }()
    
    func setupViews(){
        addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(35)
        }
        
        addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImage)
            make.left.equalTo(profileImage.snp.right).offset(8)
        }
        
        addSubview(followBtn)
        followBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-8)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
        
        contentView.addSubview(acceptBtn)
        acceptBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.right.equalTo(followBtn.snp.left).offset(-4)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

