//
//  CommentHeaderView.swift
//  Picker
//
//  Created by Vavisa Mac on 10/15/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import SwiftRichString
import ActiveLabel


protocol CommentPickedHeaderViewDelegate{
    func pickedTapped(picked_group: PickedGroup)
}

class CommentPickedHeaderView: UITableViewHeaderFooterView {
    
    var delegate:CommentHeaderViewDelegate?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupViews()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = .white
    }
    
    var picked_group:PickedGroup!{
        didSet{
            if let picked = picked_group.picked_group.first{
                if picked.is_video{
                    if let thumbUrl = picked.thumb_url{
                        if let url = URL(string: thumbUrl){
                            pickedImageView.kf.setImage(with: url)
                        }
                    }
                }else{
                    if let url = URL(string: picked.url){
                        pickedImageView.kf.setImage(with: url)
                    }
                }
            }
            
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let pickedImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.backgroundColor = UIColor.groupTableViewBackground
        return iv
    }()
    
    @objc func handlePickedImageTapped(){
        delegate?.pickedTapped(picked_group: picked_group)
    }
    
    func setupViews(){
        contentView.backgroundColor = .white
        
        contentView.addSubview(pickedImageView)
        pickedImageView.isUserInteractionEnabled = true
        pickedImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlePickedImageTapped)))
        pickedImageView.snp.makeConstraints { (make) in
            make.leading.top.bottom.equalToSuperview().inset(16)
            make.width.equalTo(125)
        }
    }
    
    
}
