//
//  CommentTableViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 2/18/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit

protocol CommentTableViewCellDelegate{
    func replyPressed(comment:Comment)
}

class ReplyTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        repliesTableView.register(ReplyTableViewCell.self, forCellReuseIdentifier: cellId)
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let cellId = "cellId"
    
    var delegate: CommentTableViewCellDelegate?
    var comment:Comment?{
        didSet{
            self.message.text = comment?.message
            
            if let time = comment?.timestampDouble{
                let date = Date(timeIntervalSince1970: time/1000)
                timeLabel.text = timeAgoSince(date)
            }
            
            self.usernameLabel.text = comment?.userProfile.username
            
            if let urlString = comment?.userProfile.profileImageUrl{
                let url = URL(string: urlString)
                self.profileImage.kf.setImage(with: url)
            }
        }
    }
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Medium", size: 14)
        label.text = "-"
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 13)
        label.text = "-"
        return label
    }()
    
    let message:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Regular", size: 13)
        label.numberOfLines = 0
        label.text = "-"
        return label
    }()
    
    let repliesTableView:UITableView = {
        let tv = UITableView()
        tv.isScrollEnabled = false
        tv.backgroundColor = .red
        return tv
    }()
    
    
    func setupViews(){
        
        
        contentView.addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(48)
            make.top.equalToSuperview().inset(12)
            make.width.height.equalTo(30)
        }
        
        contentView.addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage).inset(2)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
        contentView.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(usernameLabel)
            make.leading.equalTo(usernameLabel.snp.trailing).offset(4)
        }
        
        
        contentView.addSubview(message)
        message.snp.makeConstraints { (make) in
            make.leading.equalTo(profileImage.snp.trailing).offset(12)
            make.trailing.equalToSuperview().inset(8)
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
            make.bottom.equalToSuperview().inset(8)
        }
        
    }

}

extension ReplyTableViewCell:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ReplyTableViewCell
        return cell
    }
    
    
}


protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int)
}

class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    
    let titleLabel = UILabel()
    
    var comment:Comment?{
        didSet{
            self.message.text = comment?.message
            
            if let time = comment?.timestampDouble{
                let date = Date(timeIntervalSince1970: time/1000)
                timeLabel.text = timeAgoSince(date)
            }
            
            self.usernameLabel.text = comment?.userProfile.username
            
            if let urlString = comment?.userProfile.profileImageUrl{
                let url = URL(string: urlString)
                self.profileImage.kf.setImage(with: url)
            }
        }
    }
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Medium", size: 16)
        label.text = "-"
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 15)
        label.text = "-"
        return label
    }()
    
    let message:UILabel = {
        let label = UILabel()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Regular", size: 15)
        label.numberOfLines = 0
        label.text = "-"
        return label
    }()
    
    lazy var replyBtn:UIButton = {
        let btn = UIButton()
        let attributedTitle = NSAttributedString(string: "Reply".localized(),
                                                 attributes: [NSAttributedStringKey.foregroundColor : AppColors.shared.lightGray,
                                                              NSAttributedStringKey.font:UIFont(name: "Raleway-SemiBold", size: 15)! ])
        btn.setAttributedTitle(attributedTitle, for: .normal)
        btn.addTargetClosure(closure: { (btn) in
            //self.delegate?.replyPressed(comment: self.comment!)
        })
        btn.setTitleColor(AppColors.shared.lightGray, for: .normal)
        return btn
    }()
    
    let repliesTableView:UITableView = {
        let tv = UITableView()
        tv.isScrollEnabled = false
        tv.backgroundColor = .red
        return tv
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .white
        
        contentView.addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().inset(12)
            make.width.height.equalTo(40)
        }
        
        contentView.addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage).inset(2)
            make.leading.equalTo(profileImage.snp.trailing).offset(8)
        }
        
        contentView.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(usernameLabel)
            make.leading.equalTo(usernameLabel.snp.trailing).offset(4)
        }
        
        
        contentView.addSubview(message)
        message.snp.makeConstraints { (make) in
            make.leading.equalTo(profileImage.snp.trailing).offset(12)
            make.trailing.equalToSuperview().inset(8)
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
        }
        
        contentView.addSubview(replyBtn)
        replyBtn.snp.makeConstraints { (make) in
            make.leading.equalTo(message)
            make.top.equalTo(message.snp.bottom).offset(4)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(_:))))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    // Trigger toggle section when tapping on the header
    //
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        //
        // Animate the arrow rotation (see Extensions.swf)
        //
    }
    
}
