//
//  WhiteLineButton.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/26/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

class WhiteLineButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        titleLabel?.textColor = .white
        layer.borderWidth = 2
        layer.borderColor = UIColor.white.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height/2
    }
}


class LineButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        layer.borderWidth = 1
        layer.borderColor = UIColor.clear.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height/2
    }
}
