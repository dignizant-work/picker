//
//  CustomTextfield.swift
//  custom camera
//
//  Created by Omar Mohammed on 7/28/17.
//  Copyright © 2017 Omar Mohammed. All rights reserved.
//

import UIKit

class customTextField: UITextField, UITextFieldDelegate {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        delegate = self
        borderStyle = .none
        leftView?.tintColor = AppColors.shared.lightGray
        textColor = AppColors.shared.blue
        font = UIFont(name: "Raleway-Medium", size: 16)
        if let placeholder = self.placeholder{
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor:UIColor(white: 0.85, alpha: 1)])
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let borderLine = UIView()
        let height = 1.0
        borderLine.frame = CGRect(x: 50, y: Double(self.frame.height) - height, width: Double(self.frame.width) - 50, height: height)
        borderLine.backgroundColor = AppColors.shared.superLightGray
        self.addSubview(borderLine)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        leftView?.tintColor = AppColors.shared.blue
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        leftView?.tintColor = AppColors.shared.lightGray
    }
    
}

class CameraTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
  
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
        layer.cornerRadius = frame.height/2
        textColor = .white
        font = UIFont(name: "Raleway-SemiBold", size: 16)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let placeholder = self.placeholder{
            attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
        }
    }
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
