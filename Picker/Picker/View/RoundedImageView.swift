//
//  RoundedImageView.swift
//  custom camera
//
//  Created by Omar Mohammed on 7/20/17.
//  Copyright © 2017 Omar Mohammed. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init () {
        self.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = frame.height/2
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 2
        clipsToBounds = true
        isUserInteractionEnabled = true
        contentMode = .scaleAspectFill
    }

}

class RoundedImageViewNoBorder: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init () {
        self.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = frame.height/2
        clipsToBounds = true
        isUserInteractionEnabled = true
        contentMode = .scaleAspectFill
    }
    
}
