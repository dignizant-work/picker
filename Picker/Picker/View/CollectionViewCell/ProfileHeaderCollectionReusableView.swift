//
//  ProfileHeaderCollectionReusableView.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/25/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
//import Kingfisher
import SDWebImage

class ProfileHeaderCollectionReusableView: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        backgroundColor = .clear
    }
    
    var userProfile:UserProfile2? {
        didSet{
            if let username = userProfile?.username{usernameLabel.text = username}
            if let name = userProfile?.name{nameLabel.text = name}
            if let bio = userProfile?.bio{bioLabel.text = bio}
            if let profileImageUrl = userProfile?.profileImageUrl{
                let profileUrl = URL(string:profileImageUrl)
//                profileImage.kf.setImage(with: profileUrl, placeholder: #imageLiteral(resourceName: "Profile_placeholder"))
                profileImage.sd_setImage(with: profileUrl, placeholderImage: UIImage(named: "Profile_placeholder"), options: .lowPriority, context: nil)
            }
        }
    }
    
    //MARK:- Views
    let headerBackground:UIImageView = {
        let iv = UIImageView()
        iv.isUserInteractionEnabled = true
        iv.image = #imageLiteral(resourceName: "Profile_header_bg")
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    
    let profileImage:RoundedImageView = {
        let iv = RoundedImageView()
        iv.contentMode = .scaleAspectFill
        iv.image = #imageLiteral(resourceName: "Profile_placeholder")
        return iv
    }()
    

    let nameLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-SemiBold", size: 17)
        label.textColor = AppColors.shared.darkGray
        return label
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-Medium", size: 15)
        label.textColor = AppColors.shared.gray
        return label
    }()
    
    let bioLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 3
        label.font = UIFont(name: "Raleway-Medium", size: 14)
        label.textColor = AppColors.shared.gray
        return label
    }()
    
    let followersLabel:UILabel = {
        let label = UILabel()
        label.text = "Followers".localized()
        label.isUserInteractionEnabled = true
        label.font = UIFont(name: "Raleway-Medium", size: 14)
        label.textColor = AppColors.shared.gray
        return label
    }()
    
    let followersNumberLabel:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.font = UIFont(name: "Raleway-SemiBold", size: 16)
        label.textColor = AppColors.shared.darkGray
        return label
    }()
    
    let followingLabel:UILabel = {
        let label = UILabel()
        label.text = "Following".localized()
        label.isUserInteractionEnabled = true
        label.font = UIFont(name: "Raleway-Medium", size: 14)
        label.textColor = AppColors.shared.gray
        return label
    }()
    
    let followingNumberLabel:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.font = UIFont(name: "Raleway-SemiBold", size: 16)
        label.textColor = AppColors.shared.darkGray
        return label
    }()
    
    let line:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.superLightGray
        return v
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    func setupViews(){
        addSubview(headerBackground)

        headerBackground.addSubview(profileImage)
        headerBackground.addSubview(nameLabel)
        headerBackground.addSubview(usernameLabel)
        headerBackground.addSubview(bioLabel)
        headerBackground.addSubview(followersLabel)
        headerBackground.addSubview(followersNumberLabel)
        headerBackground.addSubview(followingLabel)
        headerBackground.addSubview(followingNumberLabel)
        headerBackground.addSubview(line)
     
        headerBackground.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }
        profileImage.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(-30)
            make.width.height.equalTo(frame.width/3.5)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(profileImage.snp.bottom).offset(12)
            
        }
        
        usernameLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(nameLabel.snp.bottom).offset(4)
        }
        
        bioLabel.snp.makeConstraints { (make) in
            make.top.equalTo(usernameLabel.snp.bottom).offset(4)
            make.centerX.equalToSuperview()
            make.right.left.equalToSuperview().inset(frame.width/6)
        }
        
        line.snp.makeConstraints { (make) in
            make.top.equalTo(bioLabel.snp.bottom).offset(16)
            make.centerX.equalToSuperview()
            make.width.equalTo(frame.width/1.5)
            make.height.equalTo(1)
        }
        followersLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(32)
            make.bottom.equalTo(profileImage).offset(-4)
        }
        followersNumberLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(followersLabel)
            make.bottom.equalTo(followersLabel.snp.top).offset(-4)
        }
        followingLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-32)
            make.bottom.equalTo(profileImage).offset(-4)
        }
        followingNumberLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(followingLabel)
            make.bottom.equalTo(followingLabel.snp.top).offset(-4)
        }
    }
}
