//
//  SearchUserCollectionViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/11/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import Kingfisher
import SDWebImage

protocol SearchUserDelegate {
    func followBtnPressed(targetUserId: Int, cell: SearchUserCollectionViewCell)
}

class SearchUserCollectionViewCell: UICollectionViewCell {

    var shadowLayer: CAShapeLayer!
    var delegate:SearchUserDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    var user:SearchUser!{
        didSet{
            
         
            usernameLabel.text = "@" + user.username
            nameLabel.text = user.name
            if let profile_image_string = user.profile_image{
                let url = URL(string: profile_image_string)
//                profileImage.kf.setImage(with: url)
                profileImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
            }
            else {
                profileImage.image = #imageLiteral(resourceName: "Profile_placeholder")
            }
            
            
            if user.is_follow{
                print("ISFOLLOW")
                self.followBtn.backgroundColor = AppColors.shared.red
                self.followBtn.setTitle("Unfollow".localized(), for: .normal)
            }else{
                print("NOT FOLLOW")
                self.followBtn.backgroundColor = AppColors.shared.blue
                self.followBtn.setTitle("Follow".localized(), for: .normal)
            }
            
            
        }
    }
    
    let shadowView:UIView = {
        let v = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    let profileImage:RoundedImageViewNoBorder = {
        let v = RoundedImageViewNoBorder()
        v.image = #imageLiteral(resourceName: "Profile_placeholder")
        return v
    }()
    
    let nameLabel:UILabel = {
        let label = UILabel()
        label.text = "name"
        label.textAlignment = .center
        label.numberOfLines = 2
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-Medium", size: 16)
        return label
    }()
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.textAlignment = .center
        label.numberOfLines = 2
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 14)
        return label
    }()
    
    lazy var followBtn:FillButton = {
        let btn = FillButton()
        btn.titleLabel?.font = UIFont(name: "Raleway-SemiBold", size: 13)
        btn.addTargetClosure(closure: { (_) in
            self.delegate?.followBtnPressed( targetUserId: (self.user?.id)!, cell: self)
        })
        return btn
    }()
    
    
    
    func setupViews(){
        
        addSubview(shadowView)
        shadowView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        shadowView.addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(16)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(50)
        }
        
        shadowView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage.snp.bottom).offset(4)
            make.right.left.equalToSuperview().inset(2)
        }
        
        shadowView.addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(2)
            make.right.left.equalToSuperview().inset(2)
        }
        
        shadowView.addSubview(followBtn)
        followBtn.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(8)
            make.width.equalTo(75)
            make.height.equalTo(25)
        }
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.shadowView.layer.cornerRadius = 5
        self.shadowView.layer.borderWidth = 1.0
        self.shadowView.layer.borderColor = AppColors.shared.lightGray.cgColor
        self.shadowView.backgroundColor = AppColors.shared.superLightGray
        
//        self.shadowView.layer.borderColor = UIColor.clear.cgColor
//        self.shadowView.layer.masksToBounds = false
//        self.shadowView.layer.cornerRadius = 15
//        
//        self.shadowView.layer.shadowColor = UIColor(white: 0, alpha: 0.24).cgColor
//        self.shadowView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        self.shadowView.layer.shadowRadius = 10
//        self.shadowView.layer.shadowOpacity = 0.7
//        self.shadowView.layer.shadowPath = UIBezierPath(roundedRect: self.shadowView.bounds, cornerRadius: self.shadowView.layer.cornerRadius).cgPath
        
    }
}

