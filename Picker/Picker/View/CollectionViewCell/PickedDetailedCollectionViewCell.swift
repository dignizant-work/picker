//
//  PickedDetailedCollectionViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/8/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import AVFoundation
import Hero
import SwiftRichString
import RealmSwift
import KDCircularProgress
import ActiveLabel
import Realm
import SDWebImage
import JPVideoPlayer

protocol PickedDetailedCollectionViewCellDelegate: class {
    func downloadTapped(cell:PickedDetailedCollectionViewCell)
    func deleteTapped(cell:PickedDetailedCollectionViewCell)
    func moreTapped(cell:PickedDetailedCollectionViewCell)
    func commentTapped(cell:PickedDetailedCollectionViewCell)
    func rightTap(cell:PickedDetailedCollectionViewCell)
    func leftTap(cell:PickedDetailedCollectionViewCell)
    func like(cell:PickedDetailedCollectionViewCell)
    func profileTapped(userId: Int)
    func mentionTapped(username: String)
    func hashtagTapped(hashTag: String)
}

class PickedDetailedCollectionViewCell: UICollectionViewCell,UIScrollViewDelegate {
 
    @IBOutlet weak var pickedImageView: UIImageView!
    @IBOutlet weak var topOverlay: UIImageView!
    @IBOutlet weak var bottomOverlay: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var captionLabel: ActiveLabel!
    @IBOutlet weak var tagLabel: ActiveLabel!
    @IBOutlet weak var pickedContentHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickedContetnWrapper: UIView!
    @IBOutlet weak var captionLabelBottonConstraint: NSLayoutConstraint!
    @IBOutlet weak var pickedContetnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftTapView: UIView!
    @IBOutlet weak var centerTapView: UIView!
    @IBOutlet weak var rightTapView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    
    var elementsHidden = false
    var isCurrentUser = false
    var videoUrlString = String()
    var pickedStruct: PickedStruct = PickedStruct()
    var isDismissed = false
    
    weak var cellDelegate: PickedDetailedCollectionViewCellDelegate?
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.isUserInteractionEnabled = false
        profileImageView.layer.cornerRadius = 25
        countLabel.layer.cornerRadius = 20
        deleteButton.alpha = 0
        downloadButton.alpha = 0
        addTapGesturesToView()
        setupCaptionLabel()
        setupTagLabel()
        addProgressView()
        setupCustomLayout()
    }
    
    fileprivate func hideBottomGradiantForiPhoneX(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2688, 1792, 2436:
                // iPhone X, XR , XS max
                if pickedStruct.tag == "" && (pickedStruct.caption == nil || pickedStruct.caption == ""){
                    bottomOverlay.isHidden = true
                }
            default: break
            }
        }
    }
        
    override func prepareForReuse() {
        bottomOverlay.isHidden = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        pickedContentHeightConstraint.constant = frame.width * 16/9
    }
 
    func configure(pickedStruct: PickedStruct, isCurrentUser: Bool, download:Download?, isFromComment:Bool){
        
        self.pickedStruct = pickedStruct
        
        // Store seen picked on local Realm database
        let seenPicked = SeenPicked()
        seenPicked.id = String(pickedStruct.id)
        let realm = try! Realm()
        try! realm.write {
            realm.add(seenPicked)
        }
        
        commentButton.isEnabled = !isFromComment
        self.isCurrentUser = isCurrentUser

        locationLabel.text = pickedStruct.location
        tagLabel.text = pickedStruct.tag
        captionLabel.text = pickedStruct.caption
        timeLabel.text = pickedStruct.readable_time
        likeButton.isSelected = pickedStruct.is_liked ?? false
        likesCountLabel.text = String(pickedStruct.likes_count)
        
        if pickedStruct.tag == ""{
            moreButton.isHidden = true
            moreButton.widthAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        if isCurrentUser{
            self.deleteButton.alpha = 1
            self.downloadButton.alpha = 1
        }
        
        if isCurrentUser{
            commentsCountLabel.text = String(pickedStruct.comments_count)
            if !pickedStruct.comments_are_public{
                self.commentButton.setImage(#imageLiteral(resourceName: "Comment disabeled"), for: .normal)
            }
        }
        else
        {
            if pickedStruct.comments_are_public{
                commentsCountLabel.text = String(pickedStruct.comments_count)
            }else{
                self.commentButton.setImage(#imageLiteral(resourceName: "Comment disabeled"), for: .normal)
            }
        }
         hideBottomGradiantForiPhoneX()
    }
    
    lazy var progress:KDCircularProgress = {
        let p = KDCircularProgress()
        p.isUserInteractionEnabled = false
        p.progressThickness = 0.1
        p.trackThickness = 0.1
        p.trackColor = AppColors.shared.gray
        p.set(colors: AppColors.shared.blue)
        return p
    }()
    
    @objc func profileTapped() {
        cellDelegate?.profileTapped(userId: pickedStruct.user_id)
    }
    
    @IBAction func downloadTapped(_ sender: Any) {
        cellDelegate?.downloadTapped(cell: self)
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        cellDelegate?.deleteTapped(cell: self)
    }
    
    @IBAction func likeTapped(_ sender: Any) {
        self.likeButton.isSelected = !self.likeButton.isSelected
        let count = Int(likesCountLabel.text!)!
        if likeButton.isSelected{
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.likeButton.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            }, completion: { (true) in
                UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.likeButton.transform = .identity
                })
            })
            
            likesCountLabel.text = String(count +  1)
        }else{
            likesCountLabel.text = String(count - 1)
        }
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.likePicked(user_id: userId, pickd_id: pickedStruct.id) { (error) in
            NotificationCenter.default.post(name: NSNotification.Name("reloadData"), object: nil)
        }
        
        cellDelegate?.like(cell: self)
    }
    
    @IBAction func commentTapped(_ sender: Any) {
        cellDelegate?.commentTapped(cell: self)
    }
    
    @IBAction func moreTapped(_ sender: Any) {
        cellDelegate?.moreTapped(cell: self)
    }
    
    fileprivate func addTapGesturesToView() {
        rightTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleRightTap)))
        
        leftTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLeftTap)))
        
        centerTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleCenterTap)))
        
        addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(cameraZoom(sender:))))
        
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
    }
    
    fileprivate func addProgressView() {
        addSubview(progress)
        progress.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(85)
        }
    }
    
    fileprivate func setupCustomLayout() {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2688, 1792, 2436:
                // iPhone X, XR , XS max
                pickedContetnWrapper.layer.cornerRadius = 10
                pickedContetnWrapper.clipsToBounds = true
                captionLabelBottonConstraint.isActive = false
                captionLabel.snp.makeConstraints { (make) in
                    make.bottom.equalTo(pickedContetnWrapper).offset(-16)
                }
            default:
                pickedContetnTopConstraint.isActive = false
                pickedContetnWrapper.snp.makeConstraints { (make) in
                    make.top.equalToSuperview()
                }
                topViewTopConstraint.isActive = false
                topView.snp.makeConstraints { (make) in
                    make.top.equalToSuperview()
                }
                break
            }
        }
    }
    
    fileprivate func setupCaptionLabel() {
        captionLabel.enabledTypes = [.mention, .hashtag]
        captionLabel.mentionColor = AppColors.shared.blue
        captionLabel.hashtagColor = AppColors.shared.blue
        captionLabel.handleMentionTap({ (username) in
            self.cellDelegate?.mentionTapped(username: username)
        })
        captionLabel.handleHashtagTap { (hashtag) in
            self.cellDelegate?.hashtagTapped(hashTag: hashtag)
        }
    }
    
    fileprivate func setupTagLabel(){
        tagLabel.enabledTypes = [.hashtag]
        tagLabel.hashtagColor = AppColors.shared.blue
        tagLabel.handleHashtagTap { (hashtag) in
            self.cellDelegate?.hashtagTapped(hashTag: hashtag)
        }
    }
    
    @objc func handleRightTap(){
        self.cellDelegate?.rightTap(cell: self)
    }
    
    @objc func handleLeftTap(){
        self.cellDelegate?.leftTap( cell: self)
    }
    
    func hideElements(){
        UIView.animate(withDuration: 0.2, animations: {
            self.topView.alpha = 0
            self.bottomView.alpha = 0
            self.bottomOverlay.alpha = 0
        })
    }
    
    func showElements(){
        UIView.animate(withDuration: 0.2, animations: {
            self.topView.alpha = 1
            self.bottomView.alpha = 1
            self.bottomOverlay.alpha = 1
        })
    }
    
    @objc func handleCenterTap(){
        if elementsHidden{
            showElements()
        }else{
            hideElements()
        }
        elementsHidden = !elementsHidden
    }
    
    @objc func cameraZoom(sender: UIPinchGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            hideElements()
            let pinchCenter = CGPoint(x: sender.location(in: self).x - self.bounds.midX,
                                      y: sender.location(in: self).y - self.bounds.midY)
            let transform = self.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                .scaledBy(x: sender.scale, y: sender.scale)
                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
            let scale = sender.scale
            if scale > 1 && scale < 3 {
                self.transform = transform
                sender.scale = 1
            }
        }
        
        if sender.state == .ended{
            showElements()
            UIView.animate(withDuration: 0.2, animations: {
               self.transform = .identity
            })
        }
    }
}


