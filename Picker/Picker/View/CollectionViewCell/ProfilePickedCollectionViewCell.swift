//
//  ProfilePickCollectionViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/25/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SDWebImage
//import Kingfisher
import Hero
import RealmSwift

protocol ProfilePickedCollectionViewCellDelegate{
    func bookmarkPressed(cell: ProfilePickedCollectionViewCell)
}

class ProfilePickedCollectionViewCell: UICollectionViewCell {
    
    var shadowLayer: CAShapeLayer!
    var pickedGroup:PickedGroup = PickedGroup()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        bookmarkBtn.tintColor = AppColors.shared.lightGray
        thumbImage.layer.borderWidth = 2
        thumbImage.layer.borderColor = AppColors.shared.blue.cgColor
    }
    
    var delegate:ProfilePickedCollectionViewCellDelegate?
    var customIdForHero  = ""
    
    func configure(pickedGroup:PickedGroup){
        self.pickedGroup = pickedGroup
        if let picked = pickedGroup.picked_group.first{
            
            if picked.is_video{
                if let thumbUrlString = picked.thumb_url{
                    let url = URL(string:thumbUrlString)
                    thumbImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .queryDiskDataSync, context: nil)
                }
            }else{
                let thumbUrlString = picked.url
                let url = URL(string:thumbUrlString)
                thumbImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .queryDiskDataSync, context: nil)
            }
            
            tagLabel.text = picked.tag
            locationLabel.text = picked.location
            contentCountLabel.text = String(pickedGroup.picked_group.count)
            timeLabel.text = picked.readable_time
            
            // check if last picked is seen
            let realm = try! Realm()
            let result = realm.objects(SeenPicked.self).filter("id == '" + String((pickedGroup.picked_group.last?.id)!) + "' ")
            if result.count > 0 {
                thumbImage.layer.borderWidth = 0
            }

            let sumOfLikes = pickedGroup.picked_group.map({$0.likes_count}).reduce(0, +)
            likesCountLabel.text = String(sumOfLikes)
            
            thumbImage.hero.id = String(pickedGroup.picked_group.first?.id ?? 0)
        }
        
        
        if pickedGroup.is_saved{
            bookmarkBtn.tintColor = AppColors.shared.yellow
        }
        
    }
    
    
    let shadowView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.lightGray
        return v
    }()
    
    let thumbImage:UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFill
        v.clipsToBounds = true
        v.layer.borderColor = AppColors.shared.blue.cgColor
        v.layer.borderWidth = 2
        return v
    }()
    
    let contentImage:UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        v.image = #imageLiteral(resourceName: "book")
        v.tintColor = AppColors.shared.blue
        return v
    }()
    
    let commentImage:UIImageView = {
        let v = UIImageView()
        v.contentMode = .scaleAspectFit
        v.image = #imageLiteral(resourceName: "comment_unselected")
        v.tintColor = AppColors.shared.blue
        return v
    }()
    
    let likeImageView:UIImageView = {
        let v = UIImageView()
        v.dropDarkShadow()
        v.contentMode = .scaleAspectFit
        v.image = #imageLiteral(resourceName: "Like_btn")
        return v
    }()
    
    let likesCountLabel:UILabel = {
        let label = UILabel()
        label.dropDarkShadow()
        label.textColor = .white
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    lazy var bookmarkBtn:UIButton = {
        let btn = UIButton()
        btn.tintColor = AppColors.shared.lightGray
        btn.setImage(#imageLiteral(resourceName: "Save"), for: .normal)
        btn.addTargetClosure(closure: { (_) in
            self.delegate?.bookmarkPressed(cell: self)
        })
        return btn
    }()
    
    let tagLabel:UILabel = {
        let label = UILabel()
        label.dropDarkShadow()
        label.numberOfLines = 2
        label.textColor = .white
        label.text = label.text?.uppercased()
        label.font =  UIFont(name: "Raleway-SemiBold", size: 15)
        return label
    }()
    
    let contentCountLabel:UILabel = {
        let label = UILabel()
        label.dropDarkShadow()
        label.textColor = .white
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    let commentCountLabel:UILabel = {
        let label = UILabel()
        label.text = "0"
        label.textColor = AppColors.shared.blue
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    let timeLabel:UILabel = {
        let label = UILabel()
        label.dropDarkShadow()
        label.textColor = .white
        label.font =  UIFont(name: "Raleway-SemiBold", size: 13)
        return label
    }()
    
    let locationImage:UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Location")
        iv.contentMode = .scaleAspectFit
        iv.tintColor = AppColors.shared.lightGray
        return iv
    }()
    
    let locationLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = AppColors.shared.lightGray
        label.font =  UIFont(name: "Raleway-Semibold", size: 14)
        label.numberOfLines = 2
        return label
    }()
    
    
    func setupViews(){
        
        addSubview(locationImage)
        locationImage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(8)
            make.height.width.equalTo(20)
        }
        
        addSubview(locationLabel)
        locationLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(locationImage.snp.trailing).offset(4)
            make.centerY.equalTo(locationImage)
        }
        
        addSubview(shadowView)
        shadowView.snp.makeConstraints { (make) in
            make.top.equalTo(locationImage.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(self.frame.width * 4/3)
        }
        
        shadowView.addSubview(thumbImage)
        thumbImage.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        
        
        thumbImage.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(12)
            make.top.equalToSuperview().offset(16)
        }
        
        thumbImage.addSubview(likeImageView)
        likeImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().offset(-8)
            make.height.width.equalTo(25)
        }
        
        thumbImage.addSubview(likesCountLabel)
        likesCountLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(likeImageView.snp.trailing).offset(8)
            make.centerY.equalTo(likeImageView)
        }
        
        thumbImage.addSubview(tagLabel)
        tagLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(12)
            make.bottom.equalTo(likeImageView.snp.top).offset(-8)
        }
        
//        addSubview(contentImage)
//        contentImage.snp.makeConstraints { (make) in
//            make.top.equalTo(thumbImage.snp.bottom).offset(12)
//            make.right.equalTo(thumbImage).offset(-8)
//            make.height.width.equalTo(25)
//            make.bottom.equalToSuperview()
//        }
        
        thumbImage.addSubview(contentCountLabel)
        contentCountLabel.snp.makeConstraints { (make) in
            make.trailing.top.equalTo(thumbImage).inset(12)
        }
        
//        addSubview(commentImage)
//        commentImage.snp.makeConstraints { (make) in
//            make.top.equalTo(thumbImage.snp.bottom).offset(12)
//            //make.right.equalTo(contentCountLabel.snp.left).offset(-8)
//            make.centerX.equalToSuperview()
//            make.height.width.equalTo(25)
//            make.bottom.equalToSuperview()
//        }
//
//        addSubview(commentCountLabel)
//        commentCountLabel.snp.makeConstraints { (make) in
//            make.right.equalTo(commentImage.snp.left).offset(-8)
//            make.centerY.equalTo(contentImage)
//        }
        
        addSubview(bookmarkBtn)
        bookmarkBtn.snp.makeConstraints { (make) in
            make.top.equalTo(thumbImage.snp.bottom).offset(12)
            make.left.equalTo(thumbImage).offset(8)
            make.height.width.equalTo(25)
            make.bottom.equalToSuperview()
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.thumbImage.layer.cornerRadius = 5
//        self.shadowView.layer.cornerRadius = 5

    }
    
}

protocol SearchPickedCollectionViewCellDelegate {
    func profileSelected(userId: Int)
}

class SearchPickedCollectionViewCell:ProfilePickedCollectionViewCell{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    var searchDelegate:SearchPickedCollectionViewCellDelegate?
    
    let usernameLabel:UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.textColor = AppColors.shared.darkGray
        label.font =  UIFont(name: "Raleway-medium", size: 15)
        label.numberOfLines = 2
        label.isUserInteractionEnabled = true
        return label
    }()
    
    let profileImage:RoundedImageViewNoBorder = {
        let iv = RoundedImageViewNoBorder()
        iv.isUserInteractionEnabled = true
        iv.backgroundColor = AppColors.shared.lightGray
        iv.image = #imageLiteral(resourceName: "Profile_placeholder")
        return iv
    }()
    
    @objc func profileTapped(){
        searchDelegate?.profileSelected(userId: (user?.id)!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var user:UserShort?{
        didSet{
            
            usernameLabel.text = user?.username
            
            if let profileImageUrl = user?.profile_image{
                let url = URL(string:profileImageUrl)
//                profileImage.kf.setImage(with: url)
                profileImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
            }
        }
    }
    
    override func setupViews() {
        
        addSubview(profileImage)
        
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        profileImage.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().inset(8)
            make.height.width.equalTo(30)
        }
        
        addSubview(usernameLabel)
        usernameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileTapped)))
        usernameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImage)
            make.leading.equalTo(profileImage.snp.trailing).offset(4)
            
        }
        
        addSubview(shadowView)
        shadowView.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage.snp.bottom).offset(8)
            make.right.left.equalToSuperview()
            make.bottom.equalToSuperview().offset(-45)
        }
        
        addSubview(thumbImage)
        thumbImage.snp.makeConstraints { (make) in
            make.top.equalTo(profileImage.snp.bottom).offset(8)
            make.right.left.equalToSuperview()
            make.bottom.equalToSuperview().offset(-45)
        }
        
        
        thumbImage.addSubview(likeImageView)
        likeImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().offset(-8)
            make.height.width.equalTo(25)
        }
        
        thumbImage.addSubview(likesCountLabel)
        likesCountLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(likeImageView.snp.trailing).offset(8)
            make.centerY.equalTo(likeImageView)
        }
        
        thumbImage.addSubview(tagLabel)
        tagLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(12)
            make.bottom.equalTo(likeImageView.snp.top).offset(-8)
        }
        
        thumbImage.addSubview(timeLabel)
        timeLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(12)
            make.top.equalToSuperview().offset(16)
        }
        
        
        
        thumbImage.addSubview(contentCountLabel)
        contentCountLabel.snp.makeConstraints { (make) in
            make.right.top.equalToSuperview().inset(12)
        }
        
        addSubview(locationLabel)
        locationLabel.snp.makeConstraints { (make) in
            make.left.equalTo(thumbImage).offset(8)
            make.right.equalTo(contentCountLabel.snp.left).offset(-4)
            make.top.equalTo(thumbImage.snp.bottom).offset(12)
        }
        
    }
    
}

class TimelinePickedCollectionViewCell:ProfilePickedCollectionViewCell{
    override func setupViews() {
        super.setupViews()
        bookmarkBtn.isHidden = true
    }
}
