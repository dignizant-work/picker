//
//  HorizontalScrollingCollectionViewCell.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/12/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

protocol usersHorizontalScrollingDelegate{
    func userSelected(user_id: Int)
    func followBtnPressed(targetUserId: Int, cell: SearchUserCollectionViewCell)
}

class UsersHorizontalScrollingCollectionViewCell: UICollectionViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let searchUserCellId = "searchUserCellId"
    var usersList = [UserProfileCellData]()
    var users = [SearchUser]()
    var profileDelegate:usersHorizontalScrollingDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        clipsToBounds = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel:UILabel = {
        let label = UILabel()
        label.text = "users".localized()
        label.textColor = AppColors.shared.gray
        label.text = label.text?.uppercased()
        label.font =  UIFont(name: "Raleway-Regular", size: 20)
        return label
    }()
    
    lazy var collection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
        c.backgroundColor = .clear
        c.delegate = self
        c.dataSource = self
        //c.alwaysBounceHorizontal = true
        c.showsHorizontalScrollIndicator = false
        return c
    }()
    
    func setupViews(){
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(8)
            make.left.right.equalToSuperview().inset(16)
        }
        
        addSubview(collection)
        collection.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(titleLabel.snp.bottom)
        }
        collection.register(SearchUserCollectionViewCell.self, forCellWithReuseIdentifier: searchUserCellId)
    }
    
    
    //MARK:- collectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let UserSearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: searchUserCellId, for: indexPath) as! SearchUserCollectionViewCell
        UserSearchCell.user = users[indexPath.item]
        UserSearchCell.delegate = self
        return UserSearchCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 8, bottom: 16, right: 8)
    }
}

extension UsersHorizontalScrollingCollectionViewCell:SearchUserDelegate{
    func followBtnPressed( targetUserId: Int, cell: SearchUserCollectionViewCell) {
        if profileDelegate != nil{
            profileDelegate?.followBtnPressed(targetUserId: targetUserId, cell: cell)
        }
    }
    
    
}
