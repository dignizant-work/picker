//
//  ResetPasswordViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 12/19/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    lazy var backBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), for: .normal)
        btn.tintColor = AppColors.shared.blue
        btn.addTargetClosure(closure: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        return btn
    }()
    
    let resetBtn:BlueShadowButton = {
        let btn = BlueShadowButton()
        btn.setTitle("Reset password".localized(), for: .normal)
        btn.addTarget(self, action: #selector(resetPassword), for: .touchUpInside)
        return btn
    }()
    
    let emailTF:customTextField = {
        let tf = customTextField()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_mail_outline_36pt"))
        tf.placeholder = "Email".localized()
        return tf
    }()
    
    let indicator:UIActivityIndicatorView = {
        let v = UIActivityIndicatorView()
        v.color = .white
        return v
    }()
    
    let errorLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-Medium", size: 12)
        label.textColor = AppColors.shared.red
        return label
    }()
    
    @objc func resetPassword(){
        
        emailTF.resignFirstResponder()
        guard let email = emailTF.text else { return }
            
        if Bundle.main.preferredLocalizations.first == "en"{
            if email == ""{
                errorLabel.text = emailTF.placeholder! + " is required"
                return
            }
        }else{
            if email == ""{
                errorLabel.text = emailTF.placeholder! + " مطلوب"
                return
            }
        }
        
        indicator.startAnimating()
        resetBtn.setTitle("", for: .normal)
        APIClient.forgotPassword(email: email) { (error) in
            DispatchQueue.main.async {
                self.indicator.stopAnimating()
                self.resetBtn.setTitle("Reset password".localized(), for: .normal)
            }
            
            if let err = error{
                self.errorLabel.textColor = AppColors.shared.red
                self.errorLabel.text = err
                return
//                DispatchQueue.main.async {
//                    self.errorLabel.text = err
//                }
            }
            
            self.errorLabel.textColor = AppColors.shared.blue
            self.errorLabel.text = "Please check your email to rest password.".localized()
            self.resetBtn.isEnabled = false
            self.resetBtn.alpha = 0.5
        }

    }
    
    
    func setupViews(){
        view.backgroundColor = .white
        view.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(32)
            make.left.equalToSuperview().offset(16)
            
        }
        
        view.addSubview(resetBtn)
        resetBtn.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.centerY).offset(16)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/2)
            make.height.equalTo(view.frame.width/8)
        }
        
        resetBtn.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(emailTF)
        emailTF.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.snp.centerY).offset(-16)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        view.addSubview(errorLabel)
        errorLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(emailTF.snp.top).offset(-16)
            make.right.left.equalToSuperview().inset(16)
        }
        emailTF.addTarget(self, action: #selector(lowercase), for: .editingChanged)
        emailTF.delegate = self
    }
    
    @objc func lowercase(){
        if let text = emailTF.text{
            emailTF.text = text.lowercased()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.emailTF.resignFirstResponder()
        return true
    }

}
