//
//  PickedDetailedViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/8/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import Hero
import AVFoundation
import Photos
import SDWebImage
import JPVideoPlayer

protocol loadMorePickedDelegate {
    func loadMorePicked()
}

protocol PickedDetailedViewControllerDelegate {
    func reloadCollectionData()
    func reload(index: IndexPath, user_id:Int?)
}

class PickedDetailedViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
   
    var delegate:PickedDetailedViewControllerDelegate?

    let pickedDetailedCellId = "pickedDetailedCellId"
    var pickedGroup = PickedGroup()
    var panGR: UIPanGestureRecognizer!
    var selectedIndexPath = IndexPath()
    var indexToScroll:IndexPath?
    var indexPath:IndexPath?
    var profileIndexPath:IndexPath?
    var isCurrentUser = false
    var viewIsAppears = false
    var downloadService = DownloadService()
    var isFromComments = false
    var isDisappear = true
    
    var playingCell: PickedDetailedCollectionViewCell!
    var pickedDataGet: PickedStruct? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        downloadService.downloadsSession = downloadsSession
        panGR = UIPanGestureRecognizer(target: self,action: #selector(handlePan(gestureRecognizer:)))
        view.addGestureRecognizer(panGR)
        setupCollectionView()
        CheckIfAlreadyDownloaded()
        CheckIfCurrentUser()
    }
    
    
    fileprivate func CheckIfCurrentUser() {
        let userId = UserDefaults.standard.integer(forKey: "userId")
        if userId == pickedGroup.user.id{
            isCurrentUser = true
        }
    }
    
    fileprivate func setupCollectionView(){
        collectionView?.register(UINib(nibName: "PickedDetailedCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: pickedDetailedCellId)
        collectionView?.isPagingEnabled = true
        collectionView?.bounces = false
        collectionView?.isScrollEnabled = false
    }
    
    fileprivate func CheckIfAlreadyDownloaded() {
        for (index, picked) in pickedGroup.picked_group.enumerated(){
            do {
                let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsPath, includingPropertiesForKeys: nil)
                fileURLs.forEach { (url) in
                    guard let savedFileName = url.absoluteString.components(separatedBy: "/").last else {return}
                    guard let pickedFileName = picked.url.components(separatedBy: "/").last else {return}
                    if savedFileName == pickedFileName{
                        pickedGroup.picked_group[index].downloadedUrl = url
                    }
                }
            } catch {
                print("Error while enumerating files \(documentsPath.path): \(error.localizedDescription)")
            }
            if picked.downloadedUrl == nil{
                self.downloadService.startDownload(picked)
            }
        }
    }
    
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewIsAppears = true
//        appearData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewIsAppears = false
        if (playingCell != nil) {
            playingCell.pickedImageView.jp_stopPlay()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let index = indexToScroll{
            self.collectionView?.scrollToItem(at: index, at: UICollectionView.ScrollPosition.right, animated: false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.isDisappear = false
        if (playingCell != nil) {
            playingCell.pickedImageView?.jp_stopPlay()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        let cell = cell as! PickedDetailedCollectionViewCell
        if cell == playingCell
        {
            playingCell.pickedImageView.jp_stopPlay()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.selectedIndexPath = indexPath
        let cell = cell as! PickedDetailedCollectionViewCell
        if pickedGroup.picked_group[indexPath.item].is_video
        {
            if let downloadedUrl = pickedGroup.picked_group[indexPath.item].downloadedUrl {
                playingCell = cell
                playingCell.pickedImageView.jp_playVideo(with: downloadedUrl)
                cell.progress.isHidden = true
            }
        }
        else
        {
            if let downloadedUrl = pickedGroup.picked_group[indexPath.item].downloadedUrl {
                cell.pickedImageView.sd_setImage(with: downloadedUrl, placeholderImage: UIImage(), options: .lowPriority, context: nil)
                cell.progress.isHidden = true
            }
        }
        
        if !isCurrentUser{
            APIClient.viewPicked(pickd_id: pickedGroup.picked_group[indexPath.item].id)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedGroup.picked_group.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pickedDetailedCellId, for: indexPath) as! PickedDetailedCollectionViewCell
        
        cell.usernameLabel.text = pickedGroup.user.name
        if let urlString = pickedGroup.user.profile_image{
            let url = URL(string: urlString)
            cell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
        }
        
        cell.cellDelegate = self
        let url = URL(string: pickedGroup.picked_group[indexPath.row].url)!
        cell.configure(pickedStruct: pickedGroup.picked_group[indexPath.item], isCurrentUser: self.isCurrentUser, download: downloadService.activeDownloads[url], isFromComment: isFromComments)
        cell.countLabel.text = "     " + String( pickedGroup.picked_group.count - indexPath.item ) + "     "
        cell.hero.id = String(pickedGroup.picked_group.first?.id ?? 0)
        cell.countLabel.isHidden = isFromComments ? true : false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return view.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // Hero dismiss interactive animation
    @objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
        
        let translation = panGR.translation(in: nil)
        let progress = translation.y /  view.bounds.height/3
        let cell = self.collectionView?.cellForItem(at: selectedIndexPath) as! PickedDetailedCollectionViewCell
        print(progress)
        
        if (playingCell != nil) {
            playingCell.pickedImageView?.jp_stopPlay()
        }
        
        switch panGR.state
        {
        case .began:
            myDismiss()
        case .changed:
            if progress  <= 0.30 {
                Hero.shared.update(CGFloat(progress))
                let currentPos = CGPoint(x: (collectionView?.center.x)! , y: translation.y/3 + (collectionView?.center.y)!)
                Hero.shared.apply(modifiers: [.position(currentPos)], to: cell)
            }
        default:
            if progress  > 0.07 {
                Hero.shared.finish()
            }else{
                Hero.shared.cancel()
            }
        }
    }
    
    func myDismiss()
    {
        if let index = indexPath{
            self.delegate?.reload(index: index, user_id: pickedGroup.user.id)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func appearData() {
        
        let cell = PickedDetailedCollectionViewCell()
        if pickedDataGet?.downloadedUrl != nil {
            if self.viewIsAppears{
                if pickedDataGet?.is_video ?? false {
//                    if (self.playingCell == cell) {
                        cell.pickedImageView?.jp_playVideo(with: (pickedDataGet?.downloadedUrl)!)
//                    }
                }
                else {
                    cell.pickedImageView.sd_setImage(with: pickedDataGet?.downloadedUrl, placeholderImage: UIImage(), options: .queryDiskDataSync, context: nil)
                }
            }
            
        }
    }
}


extension PickedDetailedViewController:PickedDetailedCollectionViewCellDelegate{
    func profileTapped(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        vc.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func like(cell: PickedDetailedCollectionViewCell) {
        
    }
    
    func commentTapped(cell:PickedDetailedCollectionViewCell) {
        let vc = CommentsViewController()
        vc.pickedId = cell.pickedStruct.id
        vc.isCurrentUser = self.isCurrentUser
        vc.hero.isEnabled = true
        vc.delegate = self
        pickedDataGet = cell.pickedStruct
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func downloadTapped( cell: PickedDetailedCollectionViewCell) {
        
        if cell.pickedStruct.is_video
        {
            if let url = cell.pickedStruct.downloadedUrl{
                
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
                }, completionHandler: { succeeded, error in
                    
                    guard error == nil, succeeded else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.showSaved()
                    }
                    
                })
            }
            
        }else{
            UIImageWriteToSavedPhotosAlbum(cell.pickedImageView.image!, nil, nil, nil)
            self.showSaved()
        }
    }
    
    func deleteTapped(cell: PickedDetailedCollectionViewCell) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        if (playingCell != nil) {
            playingCell.pickedImageView?.jp_stopPlay()
        }
        
        let  deleteButton = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { (action) -> Void in
            
            APIClient.deletePicked(picked_id: cell.pickedStruct.id, completion: { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
                
                if self.pickedGroup.picked_group.count > 1{
                    let index = self.pickedGroup.picked_group.firstIndex(where: { (p) -> Bool in
                        p.id == cell.pickedStruct.id
                    })
                    self.pickedGroup.picked_group.remove(at: index!)
                    (self.collectionView?.deleteItems(at: [cell.indexPath!]))!
                    self.delegate?.reloadCollectionData()
                }else{
                    self.delegate?.reloadCollectionData()
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action) -> Void in
            if (self.playingCell != nil) {
                self.playingCell.pickedImageView?.jp_resume()
            }
        })
        
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func moreTapped(cell: PickedDetailedCollectionViewCell) {
        let alertController = UIAlertController(title: "Report spam".localized(), message: nil, preferredStyle: .alert)
        
        if (playingCell != nil) {
            playingCell.pickedImageView?.jp_pause()
        }
        
        alertController.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Reason".localized()
        })
        
        let textField = alertController.textFields![0] as UITextField
        let  deleteButton = UIAlertAction(title: "Report".localized(), style: .destructive, handler: { (action) -> Void in
            
            let userId = UserDefaults.standard.integer(forKey: "userId")
            APIClient.reportSpam(user_id: userId, pickd_id: cell.pickedStruct.id, notes: textField.text ?? "", completion: { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
                
                self.showMessage(body: "Reported".localized())
            })
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action) -> Void in
            if (self.playingCell != nil) {
                self.playingCell.pickedImageView?.jp_resume()
            }
        })
        
        alertController.addAction(deleteButton)
        alertController.addAction(cancelButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func rightTap(cell: PickedDetailedCollectionViewCell)
    {
        let nextIndex = IndexPath(item: selectedIndexPath.row + 1, section: 0)
        if nextIndex.row == pickedGroup.picked_group.count {
            if (playingCell != nil) {
                playingCell.pickedImageView?.jp_stopPlay()
            }
            myDismiss()
            return
        }
        collectionView?.selectItem(at: nextIndex, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    func leftTap( cell: PickedDetailedCollectionViewCell) {
        let prevIndex = IndexPath(item: selectedIndexPath.row - 1 , section: 0)
        collectionView?.selectItem(at: prevIndex, animated: false, scrollPosition: .centeredHorizontally)
    }
    
    func mentionTapped(username: String) {
        
        APIClient.getUserIdFromUsername(username: username) { (userId, error) in
            if let err = error{
                self.showMessageTop(body: err)
                return
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
            vc.profile_user_id = userId
            vc.hero.isEnabled = true
            
            if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
            }else{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func hashtagTapped(hashTag: String) {
        let vc = HashtagPicksViewController(hashtag: "#" + hashTag)
        let nav = UINavigationController(rootViewController: vc)
        nav.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            nav.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            nav.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(nav, animated: true, completion: nil)
    }
}


extension PickedDetailedViewController:URLSessionDownloadDelegate{
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        
        let index = pickedGroup.picked_group.firstIndex { (picked: PickedStruct) -> Bool in
            sourceURL.absoluteString == picked.url
        }
      
        if self.isDisappear {
            let destinationURL = localFilePath(for: sourceURL)
            let fileManager = FileManager.default
            try? fileManager.removeItem(at: destinationURL)
            do {
                try fileManager.copyItem(at: location, to: destinationURL)
            } catch let error {
                print("Could not copy file to disk: \(error.localizedDescription)")
            }
            
            if let index = index{
                pickedGroup.picked_group[index].downloadedUrl = destinationURL
                DispatchQueue.main.async {
                    let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                    cell?.progress.isHidden = true
                    cell?.pickedStruct.downloadedUrl = destinationURL
                    if self.viewIsAppears{
                        if self.pickedGroup.picked_group[index].is_video
                        {
//                            if (self.playingCell == cell) {
//                                cell?.pickedImageView?.jp_playVideo(with: (cell?.pickedStruct.downloadedUrl)!)
//                            }
                            cell?.pickedImageView?.jp_playVideo(with: (cell?.pickedStruct.downloadedUrl)!)
                        }else{
                            cell?.pickedImageView.sd_setImage(with: destinationURL, placeholderImage: UIImage(), options: .queryDiskDataSync, context: nil)
                        }
                    }
                }
            }
            print("finished downloading")
        }
        print("Not download now")
        
    }

    
    /*
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        
        let index = pickedGroup.picked_group.firstIndex { (picked: PickedStruct) -> Bool in
            sourceURL.absoluteString == picked.url
        }
      
        if self.isDisappear {
            let destinationURL = localFilePath(for: sourceURL)
            let fileManager = FileManager.default
            try? fileManager.removeItem(at: destinationURL)
            do {
                try fileManager.copyItem(at: location, to: destinationURL)
            } catch let error {
                print("Could not copy file to disk: \(error.localizedDescription)")
            }
            
            if let index = index{
                pickedGroup.picked_group[index].downloadedUrl = destinationURL
                DispatchQueue.main.async {
                    let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                    cell?.progress.isHidden = true
                    cell?.pickedStruct.downloadedUrl = destinationURL
                    if self.viewIsAppears{
                        if self.pickedGroup.picked_group[index].is_video {
                            if (self.playingCell != nil) {
                                self.playingCell.pickedImageView?.jp_resume()
                            }
                        }else{
                            cell?.pickedImageView.sd_setImage(with: destinationURL, placeholderImage: UIImage(), options: .queryDiskDataSync, context: nil)
                        }
                    }
                    
                }
            }
            print("finished downloading")
        }
        print("Not download now")
        
    }
    */
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        if self.isDisappear {
            guard let url = downloadTask.originalRequest?.url,
                let download = downloadService.activeDownloads[url]  else { return }
            
            download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            
            let index = pickedGroup.picked_group.firstIndex { (pick: PickedStruct) -> Bool in
                download.picked.id == pick.id
            }
            
            // Update progress bar
            DispatchQueue.main.async {
                if let index = index{
                    let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                    cell?.progress.progress = Double(download.progress)
                }   
            }
        }
    }
}

extension PickedDetailedViewController:CommentsViewControllerDelegate{
    func decreaseCommentsCountByOne(pickedId: Int) {
        let index = pickedGroup.picked_group.firstIndex { (pick: PickedStruct) -> Bool in
            pickedId == pick.id
        }
        
        DispatchQueue.main.async {
            if let index = index{
                let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                if let countString = cell?.commentsCountLabel.text{
                    if let count = Int(countString){
                        cell?.commentsCountLabel.text = String(count-1)
                    }
                }
            }
            
        }
    }
    
    func increaseCommentsCountByOne(pickedId: Int) {
        let index = pickedGroup.picked_group.firstIndex { (pick: PickedStruct) -> Bool in
            pickedId == pick.id
        }
    
        DispatchQueue.main.async {
            if let index = index{
                let cell = self.collectionView?.cellForItem(at: IndexPath(row:index , section:0)) as? PickedDetailedCollectionViewCell
                if let countString = cell?.commentsCountLabel.text{
                    if let count = Int(countString){
                        cell?.commentsCountLabel.text = String(count+1)
                    }
                }
               
            }
            
        }
    }
}
