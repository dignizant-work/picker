//
//  ViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/18/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

class RegisterViewController: UIViewController,CLLocationManagerDelegate {

    let datePicker = UIDatePicker()
    let imagePicker = UIImagePickerController()
//    let locationManager = CLLocationManager()
//    let geocoder = CLGeocoder()
//    //let placeMark = CLPlacemark()

    
    var sevenYearsBefore: Date {
        return (Calendar.current as NSCalendar).date(byAdding: .year, value: -7, to: Date(), options: [])!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

       
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        datePicker.backgroundColor = .white
        datePicker.maximumDate = sevenYearsBefore

        imagePicker.delegate = self
        
        
//        locationManager.requestWhenInUseAuthorization()
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager.startUpdatingLocation()
//        }
        
        setupViews()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
//    func getPlacemark(forLocation location: CLLocation, completionHandler: @escaping (CLPlacemark?, String?) -> ()) {
//        let geocoder = CLGeocoder()
//
//        geocoder.reverseGeocodeLocation(location, completionHandler: {
//            placemarks, error in
//
//            if let err = error {
//                completionHandler(nil, err.localizedDescription)
//            } else if let placemarkArray = placemarks {
//                if let placemark = placemarkArray.first {
//                    completionHandler(placemark, nil)
//                } else {
//                    completionHandler(nil, "Placemark was nil")
//                }
//            } else {
//                completionHandler(nil, "Unknown error")
//            }
//        })
//
//    }
//
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        let loc = manager.location
//        print("locations = \(locValue.latitude) \(locValue.longitude) \(loc)")
//
//        getPlacemark(forLocation: loc!) { (originPlacemark, error) in
//            if let err = error {
//                print(err)
//            } else if let placemark = originPlacemark {
//                print(placemark.locality)
//            }
//        }
//    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        birthTF.text = dateFormatter.string(from: sender.date)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK:Views
    
    let scrollView:UIScrollView = {
        let sv = UIScrollView()
        return sv
    }()
    
    let contentView:UIView = {
        let v = UIView()
        return v
    }()
    
    let profileImageView:RoundedImageViewNoBorder = {
        let iv = RoundedImageViewNoBorder()
        iv.backgroundColor = AppColors.shared.superLightGray
        iv.contentMode = .scaleAspectFill
        iv.image = #imageLiteral(resourceName: "Profile_placeholder")
        iv.tintColor = AppColors.shared.lightGray
        iv.clipsToBounds = true
        return iv
    }()
    
    lazy var nameTF:customTextField = {
        let tf = customTextField()
        tf.delegate = self
        tf.placeholder = "Name".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_person_outline_36pt"))
        return tf
    }()
    
    lazy var usernameTF:customTextField = {
        let tf = customTextField()
        tf.delegate = self
        tf.autocapitalizationType = .none
        tf.placeholder = "Username".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_person_outline_36pt"))
        return tf
    }()
    
    let emailTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Email".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_mail_outline_36pt"))
        return tf
    }()
    
    let passwordTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Password".localized()
        tf.isSecureTextEntry = true
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_lock_outline_36pt"))
        return tf
    }()
    
    let confirmPasswordTF:customTextField = {
        let tf = customTextField()
        tf.placeholder = "Confirm Password".localized()
        tf.isSecureTextEntry = true
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_lock_outline_36pt"))
        return tf
    }()
    
    lazy var birthTF:customTextField = {
        let tf = customTextField()
        tf.inputView = datePicker
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_date_range_36pt"))
        tf.placeholder = "Date of birth".localized() + " " + "(Optional)".localized()
        return tf
    }()
    
    
    let registerBtn:BlueShadowButton = {
        let btn = BlueShadowButton()
        btn.setTitle("Register".localized(), for: .normal)
        btn.addTarget(self, action: #selector(createAccount), for: .touchUpInside)
        return btn
    }()
    
    lazy var agreeTerms:UIButton = {
        let btn = UIButton()
        btn.setTitle("By registering you agree to our terms & conditions".localized(), for: .normal)
        btn.titleLabel?.numberOfLines = 0
        btn.titleLabel?.font = UIFont(name: "Raleway-Medium", size: 12)
        btn.setTitleColor(AppColors.shared.blue, for: .normal)
        btn.addTargetClosure(closure: { (_) in
            let nav = UINavigationController(rootViewController: TermsAndConditionsViewController())
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        })
        return btn
    }()

    let indicator:UIActivityIndicatorView = {
        let v = UIActivityIndicatorView()
        v.color = .white
        return v
    }()
    
    let errorLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-Medium", size: 12)
        label.textColor = AppColors.shared.red
        return label
    }()
    
    lazy var backBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), for: .normal)
        btn.tintColor = AppColors.shared.blue
        btn.addTargetClosure(closure: { (_) in
           self.dismiss(animated: true, completion: nil)
        })
        return btn
    }()
    
    
    
    func setupViews(){
        view.backgroundColor = .white
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(backBtn)
        contentView.addSubview(profileImageView)
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleProfileImage)))
        contentView.addSubview(nameTF)
        contentView.addSubview(usernameTF)
        contentView.addSubview(emailTF)
        contentView.addSubview(passwordTF)
        contentView.addSubview(confirmPasswordTF)
        contentView.addSubview(birthTF)
        contentView.addSubview(registerBtn)
        contentView.addSubview(agreeTerms)
        registerBtn.addSubview(indicator)
        
        contentView.addSubview(errorLabel)
        
        for textField in [nameTF, usernameTF, emailTF, passwordTF]{
            if Bundle.main.preferredLocalizations.first == "en"{
                textField.textAlignment = .left
            }else{
                textField.textAlignment = .right
            }
        }
        
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.left.equalTo(view)
        }
        
        backBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImageView.snp.centerY)
            make.left.equalToSuperview().offset(16)
        }
        
        profileImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(64)
            make.centerX.equalToSuperview()
            make.height.width.equalTo(view.frame.width/4)
        }
        
        nameTF.snp.makeConstraints { (make) in
            make.top.equalTo(profileImageView.snp.bottom).offset(32)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        usernameTF.snp.makeConstraints { (make) in
            make.top.equalTo(nameTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        emailTF.snp.makeConstraints { (make) in
            make.top.equalTo(usernameTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        emailTF.addTarget(self, action: #selector(lowercase), for: .editingChanged);
        
        passwordTF.snp.makeConstraints { (make) in
            make.top.equalTo(emailTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        confirmPasswordTF.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        birthTF.snp.makeConstraints { (make) in
            make.top.equalTo(confirmPasswordTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        registerBtn.snp.makeConstraints { (make) in
            make.top.equalTo(birthTF.snp.bottom).offset(48)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/2)
            make.height.equalTo(view.frame.width/8)
            make.bottom.equalToSuperview().offset(-48)
        }
        
        agreeTerms.snp.makeConstraints { (make) in
            make.top.equalTo(registerBtn.snp.bottom).offset(16)
            make.right.left.equalToSuperview().inset(16)
        }
        
        indicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(25)
        }
        
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(agreeTerms.snp.bottom).offset(16)
            make.left.right.equalToSuperview().inset(8)
        }
      
    }
    
    @objc func lowercase(){
        emailTF.text = emailTF.text?.lowercased()
    }
    
    @objc func handleProfileImage(){
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    @objc func createAccount(){
        guard let email = emailTF.text?.lowercased(), let password = passwordTF.text,
            let name = nameTF.text, let username = usernameTF.text?.lowercased() , let birth = birthTF.text
            else {return}

        for textField in [nameTF, usernameTF, emailTF, passwordTF]{

            if Bundle.main.preferredLocalizations.first == "en"{
                if textField.text == ""{
                    errorLabel.text = textField.placeholder! + " is required"
                    return
                }
            }else{
                if textField.text == ""{
                    errorLabel.text = textField.placeholder! + " مطلوب"
                    return
                }
            }

        }

        if !isValid(username: username){
            errorLabel.text = "Username should be in english only and between 5-20 letters.".localized()
            return
        }
        
        if password != confirmPasswordTF.text{
            errorLabel.text = "Password and confirm password does not match.".localized()
            return
        }
        

        registerBtn.isUserInteractionEnabled = false
        registerBtn.setTitle("", for: .normal)
        indicator.startAnimating()
        
        
        var imageStr = ""
        
        if let image = profileImageView.image{
            if let imageData = image.jpegData(compressionQuality: 0.0){
                imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            }
        }
        
        let token = UserDefaults.standard.value(forKey: "token") as! String
        
        APIClient.register(name: name, username: username, email: email, password: password, profile_image: imageStr,date_of_birth: birth,  token: token) { (userId, error) in
            
            if let err = error{
                self.errorLabel.text = err
                self.registerBtn.isUserInteractionEnabled = true
                self.registerBtn.setTitle("Register".localized(), for: .normal)
                self.indicator.stopAnimating()
            }else{
                UserDefaults.standard.set(userId, forKey: "userId")
                if let window = UIApplication.shared.keyWindow{
                    window.rootViewController = appTabBarController(deepLinkId: nil)
                }
            }
        }
//        Service.shared.createAccount(name: name, username: username, email: email, password: password, birth: birth, profileImage: profileImageView.image) { (error) in
//            if let err = error {
//                self.errorLabel.text = err
//                self.registerBtn.isUserInteractionEnabled = true
//                self.registerBtn.setTitle("Register".localized(), for: .normal)
//                self.indicator.stopAnimating()
//            }else{
//                if let window = UIApplication.shared.keyWindow{
//                    window.rootViewController = appTabBarController()
//                }
//            }
//        }
        
        
    }
    
    
    func isValid(username: String) -> Bool {
        
        var returnValue = false
        let emailRegEx = "[^A-Z0-9a-z_]+"
        let countRegEx = "^[A-Z0-9a-z.-_]{5,20}$"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = username as NSString
            let results = regex.matches(in: username, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                let regex = try NSRegularExpression(pattern: countRegEx)
                let nsString = username as NSString
                let results2 = regex.matches(in: username, range: NSRange(location: 0, length: nsString.length))
                if results2.count > 0{
                    returnValue = true
                }
                
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    

}


extension RegisterViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if let editedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage{
            profileImageView.image = editedImage
        }
            
        else if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage{
            profileImageView.image = chosenImage
        }
        
        dismiss(animated:true, completion: nil)
    }
}



//*************************************
// Define max length for text field
//*************************************
extension RegisterViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 18
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.leftView?.tintColor = AppColors.shared.blue

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.leftView?.tintColor = AppColors.shared.lightGray
    }
    
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
