//
//  ViewController.swift
//  TB_TwitterHeader
//
//  Created by Yari D'areglia on 08/12/2016.

import UIKit
import Kingfisher
import OneSignal
import RealmSwift
import Hero
import SDWebImage
import MXParallaxHeader

let offset_HeaderStop:CGFloat = 40.0 // At this offset the Header stops its transformations
let offset_B_LabelHeader:CGFloat = 95.0  // At this offset the Black label reaches the Header
let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label

protocol ProfileViewControllerDelegate {
    func followTapped(isFollow: Bool, userId: Int)
}
class ProfileViewController: UIViewController, MXParallaxHeaderDelegate,UIScrollViewDelegate {
    
    @IBOutlet weak var parallaxHeaderView: UIView!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var avatarImage:UIImageView!
    @IBOutlet var headerLabel:UILabel!
    @IBOutlet var headerImageView:UIImageView?
    @IBOutlet var headerImageHeight:NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var editFollowBtn: TWTButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var followIndicator: UIActivityIndicatorView!
    
    var panGR: UIPanGestureRecognizer!
    var delegate: ProfileViewControllerDelegate?
    @IBOutlet weak var lblError: UILabel!
    
    private let refreshControl = UIRefreshControl()
    let profilePickedCellId = "profilePickedCellId"
    var pickedGroups = [PickedGroup]()
    var locked  = true
    var isCurrentUser = false
    var isTabBarProfile = false
    var profile_user_id = 0
    var current_user_id = 0
    var userProfile = UserProfile()
    var isBlockedUser = true
    
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(loadProfileData), name: NSNotification.Name("reloadData"), object: nil)
        messageLabel.text = ""
        messageLabel.font = UIFont(name: "Raleway-Bold", size: 18)
        lblError.text = ""
        lblError.font = UIFont(name: "Raleway-Bold", size: 18)
        nameLabel.text = ""
        usernameLabel.text = ""
        headerLabel.text = ""
        bioLabel.text = ""
        current_user_id = UserDefaults.standard.integer(forKey: "userId")
        
        if isTabBarProfile
        {
            profile_user_id = current_user_id
            backBtn.addTargetClosure(closure: { (_) in
                self.navigationController?.popViewController(animated: true)
            })
        }
        else
        {
            backBtn.addTargetClosure(closure: { (_) in
//                self.removeHeroIhFromCellsBeforeDismiss()
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            })
        }
        
        // CURRENT USER
        if current_user_id == profile_user_id {
            isCurrentUser = true
        }
        
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            backBtn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_right_36pt"), for: .normal)
        }
        
        if isCurrentUser{
            followersLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFollowers)))
            followingLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showFollowing)))
        }
        
        panGR = UIPanGestureRecognizer(target: self,action: #selector(handlePan(gestureRecognizer:)))
        view.addGestureRecognizer(panGR)
        view.clipsToBounds = true
        
        collectionView.register(ProfilePickedCollectionViewCell.self, forCellWithReuseIdentifier: profilePickedCellId)
        collectionView.register(UINib(nibName: "ProfileCollectionTitleCell", bundle: nil), forCellWithReuseIdentifier: "ProfileCollectionTitleCell")
        navigationController?.isNavigationBarHidden = true
        setupParallaxHeader()
        self.setupRefrehser()
        [self.messageLabel, self.lblError].forEach { (lbl) in
            lbl?.textColor =  AppColors.shared.gray
            lbl?.font = UIFont(name: "Raleway-Medium", size: 17)
        }
        
        if isTabBarProfile {
            self.backBtn.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadProfileData()
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            let statusbarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: statusBarHeight))
            statusbarView.backgroundColor = AppColors.shared.blue
            view.addSubview(statusbarView)
        }
    }
    
    func setupParallaxHeader()
    {
        self.headerLabel.alpha = 0
        parallaxHeaderView.snp.makeConstraints { (make) in
            make.height.equalTo(170)
            make.width.equalTo(UIScreen.main.bounds.size.width)
        }
        headerImageView!.blurView.setup(style: UIBlurEffect.Style.light, alpha: 1).enable()
        headerImageView!.blurView.alpha = 0
        
        collectionView.parallaxHeader.view = parallaxHeaderView
        collectionView.parallaxHeader.mode = .bottom
        collectionView.parallaxHeader.height = 170
        collectionView.parallaxHeader.minimumHeight = 90
        collectionView.parallaxHeader.delegate = self
    }
    
    // MARK: - MXParallaxHeaderDelegate
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        self.avatarImage.alpha = parallaxHeader.progress
        headerImageView!.blurView.alpha = 1 - parallaxHeader.progress
        self.headerLabel.alpha = 1 - parallaxHeader.progress
        if parallaxHeader.progress <= 1.0 && parallaxHeader.progress >= 0.0
        {
            var avatarTransform = CATransform3DIdentity
            avatarTransform = CATransform3DScale(avatarTransform,(0 + parallaxHeader.progress),(0 + parallaxHeader.progress), 0)
            avatarImage.layer.transform = avatarTransform
        }
        if parallaxHeader.progress >= 1.0 && parallaxHeader.progress <= 10.5
        {
            self.headerImageHeight.constant = 170 * (0 + parallaxHeader.progress)
        }
    }
    
    fileprivate func removeHeroIhFromCellsBeforeDismiss(){
        for cell in collectionView.visibleCells as! [ProfilePickedCollectionViewCell]{
            cell.thumbImage.hero.id = ""
        }
    }
    
    // Hero dismiss interactive animation
    @objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
        let translation = panGR.translation(in: nil)
        var progress = CGFloat()
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            progress = -translation.x / view.bounds.width
        }else{
            progress = translation.x / view.bounds.width
        }
        
        switch panGR.state {
            
        case .began:
//            removeHeroIhFromCellsBeforeDismiss()
            dismiss(animated: true, completion: nil)
            
        case .changed:
            // calculate the progress based on how far the user moved
            Hero.shared.update(CGFloat(progress))
        default:
            // end the transition when user ended their touch
            if progress  > 0.25 {
                Hero.shared.finish()
            }else{
                Hero.shared.cancel()
            }
        }
    }
    
    func setupRefrehser()
    {
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    @objc func handleRefresh(){
       loadProfileData()
    }
    
    @IBAction func draftPressed(_ sender: Any) {
        let vc = DraftViewController()
        let nav = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nav, animated: false, completion: nil)
//        self.present(vc, animated: true, completion: nil)
        
        /*
         let navigationController = UINavigationController(rootViewController: controller)
         //                navigationController.modalPresentationStyle = .overCurrentContext
                         self.navigationController?.present(navigationController, animated: true, completion: nil)
         */
    }
    
    @IBAction func morePressed(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)
        alertController.addAction(cancelAction)
        
        if isCurrentUser{
            let logoutAction = UIAlertAction(title: "Logout".localized(), style: .destructive) { (action) in
                self.logout()
            }
            
            let blockedUsersAction = UIAlertAction(title: "Blocked Users".localized(), style: .default) { (action) in
//                let vc = BlockedUsersViewController()
//                self.present(vc, animated: true, completion: nil)
                
                let controller = BlockedUsersViewController()
                let navigationController = UINavigationController(rootViewController: controller)
//                navigationController.modalPresentationStyle = .overCurrentContext
                self.navigationController?.present(navigationController, animated: true, completion: nil)

                
            }
            
            let changePasswordAction = UIAlertAction(title: "Change password".localized(), style: .default) { (action) in
                let vc = ChangePasswordViewController()
                let nav = UINavigationController(rootViewController: vc)
                self.present(nav, animated: true, completion: nil)
            }
             
            let privacyPolicyAction = UIAlertAction(title: "Privacy Policy".localized(), style: .default) { (action) in
                let vc = PrivacyPolicyViewController()
                let nav = UINavigationController(rootViewController: vc)
                self.present(nav, animated: true, completion: nil)
            }
            
            alertController.addAction(blockedUsersAction)
            alertController.addAction(changePasswordAction)
            alertController.addAction(privacyPolicyAction)
            alertController.addAction(logoutAction)
            present(alertController, animated: true, completion: nil)
        }else{
            
            guard let isBlocked = userProfile.is_blocked else {return}
            let userId = UserDefaults.standard.integer(forKey: "userId")
            let blockAction = UIAlertAction(title: isBlocked ? "Unblock".localized() : "Block".localized(), style: .destructive) { (action) in
                APIClient.blockUser(user_id: userId, blocked_id: self.userProfile.id, completion: { (error) in
                    if let err = error{
                        print(err)
                        return
                    }
                    GlobalFunctions.updateTimelinePicked(profile_user_id: userId, tabBarController: self.tabBarController)
                    GlobalFunctions.updateUserProfile(profile_user_id: userId, tabBarController: self.tabBarController)
                    self.loadProfileData()
                })
            }
            
            alertController.addAction(blockAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    @objc func logout(){
        if let token = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId{
            APIClient.logout(token: token, completion: { (error) in
                if let err = error{
                    print(err)
                    return
                }
                
                UserDefaults.standard.set(0, forKey: "userId")
                OneSignal.setSubscription(false)
                
                let loginVC = LoginViewController()
                loginVC.hero.isEnabled = true
                loginVC.hero.modalAnimationType = .fade
                self.tabBarController?.hero.replaceViewController(with: loginVC)
            })
        }
    }
    
    
    @objc func loadProfileData(){
        
        if !Reachability.isConnectedToNetwork(){
            showMessageTop(body: "No internet connection".localized())
            return
        }
    
        APIClient.getUserProfile(user_id: current_user_id, target_user_id: profile_user_id) { (userProfile, error) in
            
            if let err = error {
                print(err)
                return
            }
            
            if let isBlocked = userProfile.is_blocked
            {
                if isBlocked
                {
                    /**
                     * User is blocked
                     */
                    self.isBlockedUser = true
                    self.userProfile = userProfile
                    self.nameLabel.text = userProfile.name
                    self.headerLabel.text = userProfile.name
                    self.usernameLabel.text = "@" + userProfile.username
                    self.bioLabel.text = userProfile.bio
                    if let profileImage = userProfile.profile_image{
                        if let url = URL(string: profileImage){
                            self.avatarImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
                        }
                    }
                    else {
                        self.avatarImage.image = #imageLiteral(resourceName: "Profile_placeholder")
                    }
                    if let coverImage = userProfile.cover_image{
                        if let url = URL(string: coverImage){
                            self.headerImageView?.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
                        }
                    }
                    self.editFollowBtn.setTitle("Blocked".localized(), for: .normal)
                    self.editFollowBtn.isUserInteractionEnabled = false
                    self.activityIndicator.stopAnimating()
                    self.messageLabel.text = "User not found".localized()
                    self.lblError.text = "User not found".localized()
                    //Vishal
                    let indexPath = IndexPath(row: 0, section: 0)
                    let cell = self.collectionView.cellForItem(at: indexPath) as! ProfileCollectionTitleCell
                    cell.btnFollow.setTitle("Blocked".localized(), for: .normal)
                    self.editFollowBtn.isUserInteractionEnabled = false
                    
                    self.pickedGroups.removeAll()
                    self.collectionView.reloadData()
                    
                    return
                }
            }
            /**
             * User not blocked
             */
            self.isBlockedUser = false
            self.userProfile = userProfile
            self.followingLabel.text = String(userProfile.following_count) + " " + "Following".localized()
            self.followersLabel.text = String(userProfile.followers_count) + " " + "Followers".localized()
            self.nameLabel.text = userProfile.name
            self.headerLabel.text = userProfile.name
            self.usernameLabel.text = "@" + userProfile.username
            self.bioLabel.text = userProfile.bio
            self.messageLabel.text = ""
            self.lblError.text = ""
            if let profileImage = userProfile.profile_image{
                if let url = URL(string: profileImage){
                    self.avatarImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
                }
            }
            else {
                self.avatarImage.image = #imageLiteral(resourceName: "Profile_placeholder")
            }
            if let coverImage = userProfile.cover_image{
                if let url = URL(string: coverImage){
                    self.headerImageView?.sd_setImage(with: url, placeholderImage: nil, options: .queryDiskDataSync, context: nil)
                }
            }
            if self.isCurrentUser{
                self.editFollowBtn.setTitle("Edit".localized(), for: .normal)
                self.editFollowBtn.addTarget(self, action: #selector(self.handleEdit), for: .touchUpInside)
            }
            else
            {
                if let is_follow = userProfile.is_follow{
                    self.editFollowBtn.addTarget(self, action: #selector(self.handleFollowUnfollow), for: .touchUpInside)
                    if is_follow == true{
                        self.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
                    }else{
                        self.editFollowBtn.setTitle("Follow".localized(), for: .normal)
                        
                    }
                }
            }
                    
            APIClient.getUserPicked(user_id: self.current_user_id, target_user_id: self.profile_user_id) { (pickedGroups, error) in
                if let err = error {
                    print(err)
                    return
                }
                DispatchQueue.main.async {
                    self.pickedGroups.removeAll()
                    self.pickedGroups.append(contentsOf: pickedGroups)
                    //self.downloadPickedGroups()
                    if pickedGroups.count == 0{
                        self.messageLabel.text = "There are no picked.".localized()
                        self.lblError.text = "There are no picked.".localized()
                    }else{
                        self.messageLabel.text = ""
                        self.lblError.text = ""
                    }
                    self.reloadCollectionView()
                }
            }
        }
    }
    
    func reloadCollectionView()
    {
        self.activityIndicator.stopAnimating()
        self.collectionView.reloadData()
        self.locked = false
    }
    
    @objc func handleEdit(){
        let vc = EditProfileViewController()
        vc.userProfile = self.userProfile
        vc.profileDelegate = self
        let nav = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @objc func handleFollowUnfollow()
    {
        if editFollowBtn.titleLabel?.text == "Follow".localized(){
            handleFollow()
        }else if editFollowBtn.titleLabel?.text == "Unfollow".localized(){
            // Handle Unfollow
            handleUnfollow()
        }
    }
    
    @objc func handleFollow()
    {
        //Vishal
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.collectionView.cellForItem(at: indexPath) as! ProfileCollectionTitleCell
        
        cell.followActivityIndicater.startAnimating()
        GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: profile_user_id, tabBarController: tabBarController)
        APIClient.follow(user_id: current_user_id, follow_id: profile_user_id) { (error) in
            
            if let err = error{
                print(err)
                self.editFollowBtn.setTitle("Follow".localized(), for: .normal)
                cell.btnFollow.setTitle("Follow".localized(), for: .normal)
                cell.followActivityIndicater.stopAnimating()
                return
            }
            
            GlobalFunctions.updateFollowButtons(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
            GlobalFunctions.updateTimelinePicked(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
            self.followIndicator.stopAnimating()
            self.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
            cell.btnFollow.setTitle("Unfollow".localized(), for: .normal)
            cell.followActivityIndicater.stopAnimating()
        }
    }
    
    @objc func handleUnfollow()
    {
        //Vishal
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.collectionView.cellForItem(at: indexPath) as! ProfileCollectionTitleCell
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let unfollow = UIAlertAction(title: "Unfollow".localized(), style: .destructive, handler: { (_) in
            GlobalFunctions.addIndicatorToAllFollowButtons(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
            cell.followActivityIndicater.startAnimating()
            APIClient.follow(user_id: self.current_user_id, follow_id: self.profile_user_id) { (error) in
                                
                if let err = error{
                    print(err)
                    self.editFollowBtn.setTitle("Unfollow".localized(), for: .normal)
                    cell.btnFollow.setTitle("Unfollow".localized(), for: .normal)
                    cell.followActivityIndicater.stopAnimating()
                    return
                }
                
                GlobalFunctions.updateFollowButtons(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
                GlobalFunctions.updateTimelinePicked(profile_user_id: self.profile_user_id, tabBarController: self.tabBarController)
                cell.btnFollow.setTitle("Follow".localized(), for: .normal)
                cell.followActivityIndicater.stopAnimating()
            }
        })
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        alertController.addAction(unfollow)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func showFollowers(){
        let vc = FollowListTableViewController()
        vc.titleLabel.text = "Followers".localized()
        vc.followListType = .followers
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func showFollowing(){
        let vc = FollowListTableViewController()
        vc.titleLabel.text = "Following".localized()
        vc.followListType = .following
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if refreshControl.isRefreshing{
            self.refreshControl.endRefreshing()
        }
    }
}

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    //MARK:- COLLECTION VIEW METHODS
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedGroups.count+1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 {
            return CGSize(width: view.frame.width-32, height: 180)
        }
        if pickedGroups.count == 1 {
            return CGSize(width: view.frame.width/2 - 32, height: (view.frame.width/2 - 24) + 108)// 12+16+48+8+20+4)
        }
        
        return CGSize(width: view.frame.width/2 - 24, height: (view.frame.width/2 - 24) + 108)// 12+16+48+8+20+4)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Vishal new add
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCollectionTitleCell", for: indexPath) as! ProfileCollectionTitleCell
            
            cell.btnFollowers.setTitle(String(userProfile.followers_count) + " " + "Followers".localized(), for: .normal)
            cell.btnFollowing.setTitle(String(userProfile.following_count) + " " + "Following".localized(), for: .normal)
            
            cell.lblName.text = self.userProfile.name
            cell.lblUsername.text = "@"+self.userProfile.username
            cell.lblBio.text = self.userProfile.bio ?? ""
            
            if isCurrentUser{
                cell.btnFollowers.addTarget(self, action: #selector(showFollowers), for: .touchUpInside)
                cell.btnFollowing.addTarget(self, action: #selector(showFollowing), for: .touchUpInside)
                cell.btnDraft.isHidden = false
                cell.btnMore.isHidden = false
                cell.btnDraft.addTarget(self, action: #selector(draftPressed(_:)), for: .touchUpInside)
                cell.btnMore.addTarget(self, action: #selector(morePressed(_:)), for: .touchUpInside)
            }
            else {
                cell.btnDraft.isHidden = true
                cell.btnMore.isHidden = false
                cell.btnMore.addTarget(self, action: #selector(morePressed(_:)), for: .touchUpInside)
            }
            
            if self.isCurrentUser{
                cell.btnFollow.setTitle("Edit".localized(), for: .normal)
                cell.btnFollow.addTarget(self, action: #selector(self.handleEdit), for: .touchUpInside)
            }else{
                if let is_follow = userProfile.is_follow{
                    cell.btnFollow.addTarget(self, action: #selector(self.handleFollowUnfollow), for: .touchUpInside)
                    
                    if !self.isBlockedUser {
                        if is_follow == true{
                            cell.btnFollow.setTitle("Unfollow".localized(), for: .normal)
                        }else{
                            cell.btnFollow.setTitle("Follow".localized(), for: .normal)
                        }
                    }
                }
            }
            return cell
        }
        
        let profilePickedCell = collectionView.dequeueReusableCell(withReuseIdentifier: profilePickedCellId, for: indexPath) as! ProfilePickedCollectionViewCell
        profilePickedCell.delegate = self
        profilePickedCell.thumbImage.layer.borderWidth = 0
        profilePickedCell.configure(pickedGroup: pickedGroups[indexPath.item-1])
        if !isCurrentUser{
            profilePickedCell.bookmarkBtn.isHidden = true
        }
        return profilePickedCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if pickedGroups.count == 1 {
            return 0
        }
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if pickedGroups.count == 1 {
            return 16
        }
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if pickedGroups.count == 1 {
            return UIEdgeInsets(top: 16, left: 16, bottom: 10, right: collectionView.frame.width/1-16)
        }
        return UIEdgeInsets(top: 16, left: 16, bottom: 10, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
        
        if indexPath.row == 0 {
            return
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let vc = PickedDetailedViewController(collectionViewLayout: layout)
        vc.delegate = self
        vc.hero.isEnabled = true
        vc.pickedGroup = pickedGroups[indexPath.item-1]
        vc.indexPath = indexPath
        vc.isCurrentUser = self.isCurrentUser
        vc.collectionView?.reloadData()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}


extension ProfileViewController:PickedDetailedViewControllerDelegate{
    
    func reload(index: IndexPath, user_id: Int?) {
        
    }
    
    func reloadCollectionData() {
        loadProfileData()
    }
}


extension ProfileViewController:EditProfileViewControllerDelegate{
    func getUserProfile(userProfile: UserProfile) {
        self.usernameLabel.text = "@" + userProfile.username
        self.bioLabel.text = userProfile.bio
        self.nameLabel.text = userProfile.name
        
        if let profileImage = userProfile.profile_image{
            if let url = URL(string: profileImage){
                self.avatarImage.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
            }
        }
        else {
            self.avatarImage.image = #imageLiteral(resourceName: "Profile_placeholder")
        }
        
        if let coverImage = userProfile.cover_image{
            if let url = URL(string: coverImage){
                self.headerImageView?.sd_setImage(with: url, placeholderImage: UIImage(), options: .lowPriority, context: nil)
            }
        }
        self.userProfile = userProfile
    }
}

extension ProfileViewController:ProfilePickedCollectionViewCellDelegate{
    
    func bookmarkPressed(cell: ProfilePickedCollectionViewCell) {
        
        if cell.bookmarkBtn.tintColor == AppColors.shared.lightGray{
            cell.bookmarkBtn.tintColor = AppColors.shared.yellow
            
            APIClient.savePickedGroup(picked_group_id: cell.pickedGroup.picked_group_id, completion: { (error) in
                if let err = error{
                    print(err)
                    return
                }
                
                self.showMessage(body: "Saved".localized())
            })
           
        }else{
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let unsave = UIAlertAction(title: "Unsave".localized(), style: .destructive, handler: { (_) in
                cell.bookmarkBtn.tintColor = AppColors.shared.lightGray
                APIClient.savePickedGroup(picked_group_id: cell.pickedGroup.picked_group_id, completion: { (error) in
                    if let err = error{
                        print(err)
                        return
                    }
                })
            })
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
            alertController.addAction(unsave)
            alertController.addAction(cancel)
            present(alertController, animated: true, completion: nil)
        }
    }
}




//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        let offset = scrollView.contentOffset.y
//        print(offset)
//        var avatarTransform = CATransform3DIdentity
//        var headerTransform = CATransform3DIdentity
//        // PULL DOWN -----------------
//        if offset < 0 {
//            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
//            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height)/2.0
//            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
//            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
//            header.layer.transform = headerTransform
//        }
//        // SCROLL UP/DOWN ------------
//        else
//        {
//            // Header -----------
//
//            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
//
//
//            //  ------------ Label
//
//            let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0)
//            headerLabel.layer.transform = labelTransform
//
//            //  ------------ Blur
//
//            headerBlurImageView?.alpha = min (1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
//
//            // Avatar -----------
//
//            let avatarScaleFactor = (min(offset_HeaderStop+24, offset)) / avatarImage.bounds.height * 1.1 // Slow down the animation
//            let avatarSizeVariation = ((avatarImage.bounds.height * (1.0 + avatarScaleFactor)) - avatarImage.bounds.height) / 2.0
//            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
//            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
//
//            if offset <= offset_HeaderStop {
//
//                if avatarImage.layer.zPosition < header.layer.zPosition{
//                    header.layer.zPosition = 0
//                }
//
//            }else {
//                if avatarImage.layer.zPosition >= header.layer.zPosition{
//                    header.layer.zPosition = 2
//                    backBtn.layer.zPosition = 3
//                }
//            }
//        }
//
//        // Apply Transformations
//
//        header.layer.transform = headerTransform
//        avatarImage.layer.transform = avatarTransform
//    }
//

