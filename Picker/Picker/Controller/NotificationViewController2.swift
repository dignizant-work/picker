//
//  TestViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import FirebaseAuth

class NotificationViewController: UIViewController{
    
    private let refreshControl = UIRefreshControl()
    private let followCellId = "followCellId"
    private let commentCellId = "commentCellId"
    private let pickedCellId = "pickedCellId"
    private let replyCellId = "replyCellId"
    private var notificationList = [NotificationCellData]()
    private var notifications = [NotificationStruct]()
    private var counterForUpdate = 0
    private var isRefreshing = false
//    private var isLoading = false
//    private var currentKey = ""
    
    var activityIndicator = UIActivityIndicatorView()
    let effectView = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        refreshControl.tintColor = AppColors.shared.blue
        indicator.startAnimating()
        setupViews()
        
        tableView.separatorStyle = .none
        tableView.register(NotificationFollowTableViewCell.self, forCellReuseIdentifier: followCellId)
        tableView.register(NotificationCommentTableViewCell.self, forCellReuseIdentifier: commentCellId)
        tableView.register(NotificationPickedTableViewCell.self, forCellReuseIdentifier: pickedCellId)
        tableView.register(NotificationReplyTableViewCell.self, forCellReuseIdentifier: replyCellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.items?[3].badgeValue = nil
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        
        /**
         * When loading notification view controller from tab bar
         * Check refresher end refreshing
         * Stop activity indicator
         */
        if !isRefreshing{
            indicator.stopAnimating()
        }
    }
    
    
    @objc func handleRefresh(){
        if !isRefreshing{
            loadData()
            isRefreshing = true
        }
        
    }
    
    func incrementBadgeByOne(){
        if let badgeValue = self.tabBarController?.tabBar.items?[3].badgeValue,
            let value = Int(badgeValue) {
            self.tabBarController?.tabBar.items?[3].badgeValue = String(value + 1)
        } else {
            self.tabBarController?.tabBar.items?[3].badgeValue = "1"
        }
    }
    
    
    // This function is called from tab bar controller
    func loadData(){
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.getUserNotifications(user_id: userId) { (notifications, error) in
            
            if let err = error{
                print(err)
                return
            }
            
            self.notifications.removeAll()
            self.notifications.append(contentsOf: notifications)
            
            if notifications.count == 0 {
                self.tableView.addSubview(self.noNotificationLabel)
                self.noNotificationLabel.snp.makeConstraints({ (make) in
                    make.center.equalToSuperview()
                })
            }else{
                self.noNotificationLabel.removeFromSuperview()
            }
            
        
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.indicator.stopAnimating()
                self.isRefreshing = false
                self.refreshControl.endRefreshing()
            }
            
            
            /* Badge count number */
            self.tabBarController?.tabBar.items?[3].badgeValue = nil
            for (notification) in notifications{
                if !notification.is_seen{
                    self.incrementBadgeByOne()
                }
            }
            
            APIClient.seeNotification(user_id: userId) { (error) in
                if let err = error{
                    print(err)
                    return
                }
            }
            
            print(notifications)
        }
//        guard let userId = Auth.auth().currentUser?.uid else { return  }
//        Service.shared.getNotificationList(userId: userId) { (notificationList,isEmpty) in
//
//            if notificationList.count == 0{
//                self.listTableView.addSubview(self.noNotificationLabel)
//                self.noNotificationLabel.snp.makeConstraints({ (make) in
//                    make.center.equalToSuperview()
//                })
//                self.notificationList.removeAll()
//
//                DispatchQueue.main.async {
//                    self.listTableView.reloadData()
//                    self.indicator.stopAnimating()
//                    self.isRefreshing = false
//                    self.refreshControl.endRefreshing()
//                }
//                return
//            }
//
//
//            self.noNotificationLabel.removeFromSuperview()
//            self.notificationList.removeAll()
//            self.notificationList.append(contentsOf: notificationList)
//
//            for n in notificationList{
//                if n.notification?.seen == false{
//                    DispatchQueue.main.async {
//                      self.incrementBadgeByOne()
//                    }
//                }
//            }
//
//            DispatchQueue.main.async {
//                self.listTableView.reloadData()
//                self.indicator.stopAnimating()
//                self.isRefreshing = false
//
//                if self.refreshControl.isRefreshing{
//                    self.refreshControl.endRefreshing()
//                    Service.shared.seeNotifications(completion: { (error) in
//                        if let err = error{
//                            print(err)
//                        }
//                    })
//                }
//            }

            
//        }
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
    }
    
    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    let viewTitle:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text = "Notifications".localized()
        return label
    }()
    
    lazy var tableView:UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()
    
    let indicator:UIActivityIndicatorView = {
        var i = UIActivityIndicatorView()
        i.activityIndicatorViewStyle = .gray
        return i
    }()
    
    let noNotificationLabel:UILabel = {
        let label = UILabel()
        label.text = "No Notifications.".localized()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    
    
    func setupViews(){
        view.addSubview(blueView)
        blueView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            if UIDevice().userInterfaceIdiom == .phone {
                if  UIScreen.main.nativeBounds.height ==  2436{
                    make.height.equalTo(88)
                }else{
                    make.height.equalTo(64)
                }
            }
        }
        
        blueView.addSubview(viewTitle)
        viewTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(12)
        }
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
        
    }
    
    
    
    func showActivityIndicator() {
    
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
 
        effectView.frame = CGRect(x: view.frame.midX - 23, y: view.frame.midY - 23, width: 46, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        effectView.backgroundColor = AppColors.shared.blue
        effectView.alpha = 0.8
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.frame = CGRect(x: view.frame.midX - 23, y: view.frame.midY - 23, width: 46, height: 46)
        
        activityIndicator.startAnimating()
        
        view.addSubview(effectView)
        view.addSubview(activityIndicator)
    }
    
    func hideActivityIndicator(){
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
    }
    
    
    
    
}

extension NotificationViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch notifications[indexPath.row].type{
        case "follow":
            let cell = tableView.dequeueReusableCell(withIdentifier: followCellId, for: indexPath) as! NotificationFollowTableViewCell
            cell.notificationFollowDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        case "comment":
            let cell = tableView.dequeueReusableCell(withIdentifier: commentCellId, for: indexPath) as! NotificationCommentTableViewCell
            cell.notificationCommentDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        case "picked":
            let cell = tableView.dequeueReusableCell(withIdentifier: pickedCellId, for: indexPath) as! NotificationPickedTableViewCell
            cell.notificationPickedDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        case "reply":
            let cell = tableView.dequeueReusableCell(withIdentifier: replyCellId, for: indexPath) as! NotificationReplyTableViewCell
            cell.notificationReplyDelegate = self
            cell.notification = notifications[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            APIClient.deleteNotification(notification_id: notifications[indexPath.row].id) { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
            }
            
            self.notifications.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastSectionIndex = tableView.numberOfSections - 1
//        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
//        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
//            // print("this is the last cell")
//            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//            spinner.startAnimating()
//            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//            self.listTableView.tableFooterView = spinner
//            self.listTableView.tableFooterView?.isHidden = false
//            loadMore()
//        }
//    }
    

}

extension NotificationViewController:NotificationFollowTableViewCellDelegate{
    
    func profileTapped(cell: NotificationFollowTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func acceptBtnPressed(targetUserId: Int, cell:NotificationFollowTableViewCell) {
        
        if cell.acceptBtn.titleLabel?.text == "Accept".localized(){
            cell.acceptBtn.layer.borderColor = AppColors.shared.red.cgColor
            cell.acceptBtn.setTitleColor(AppColors.shared.red, for: .normal)
            cell.acceptBtn.setTitle("Unaccept".localized(), for: .normal)
        }else{
            cell.acceptBtn.layer.borderColor = AppColors.shared.blue.cgColor
            cell.acceptBtn.setTitleColor(AppColors.shared.blue, for: .normal)
            cell.acceptBtn.setTitle("Accept".localized(), for: .normal)
        }
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.acceptFollower(user_id: targetUserId, follow_id: userId) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
    }
    
    func followBtnPressed( targetUserId: Int, cell: NotificationFollowTableViewCell) {
        
        
        if cell.followBtn.titleLabel?.text == "Follow".localized(){
            cell.followBtn.backgroundColor = AppColors.shared.red
            cell.followBtn.setTitle("Unfollow".localized(), for: .normal)
        }else{
            cell.followBtn.backgroundColor = AppColors.shared.blue
            cell.followBtn.setTitle("Follow".localized(), for: .normal)
        }
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.follow(user_id: userId, follow_id: targetUserId) { (error) in
            if let err = error{
                print(err)
                return
            }
        }
    }
    
}


extension NotificationViewController:NotificationCommentTableViewCellDelegate{
    
    func commentTapped(cell: NotificationCommentTableViewCell) {
        
        let vc = CommentsViewController()
        
        if let pickedId = cell.notification?.picked_id{
             vc.pickedId = pickedId
        }
       
        if let commentId = cell.notification?.comment_id{
            vc.commentIdToScrollTo = commentId
        }
        
        if let pickedUserId = cell.notification?.picked_user_id{
            if pickedUserId == UserDefaults.standard.integer(forKey: "userId"){
                vc.isCurrentUser = true
            }else{
                vc.isCurrentUser = false
            }
        }
        

        vc.isHeroEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
             vc.heroModalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
             vc.heroModalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
     
    func profileTapped(cell: NotificationCommentTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}


extension NotificationViewController:NotificationReplyTableViewCellDelegate{
    func profileTapped(cell: NotificationReplyTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func commentTapped(cell: NotificationReplyTableViewCell) {
        
        let vc = CommentsViewController()
        
        if let pickedId = cell.notification?.picked_id{
            vc.pickedId = pickedId
        }
        
        if let commentId = cell.notification?.comment_id{
            vc.commentIdToScrollTo = commentId
        }
        
        if let pickedUserId = cell.notification?.picked_user_id{
            if pickedUserId == UserDefaults.standard.integer(forKey: "userId"){
                vc.isCurrentUser = true
            }else{
                vc.isCurrentUser = false
            }
        }
        
        vc.isNotificationReply = true
        vc.isHeroEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.heroModalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.heroModalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    
}


extension NotificationViewController: NotificationPickedTableViewCellDelegate{
    
    func profileTapped(cell: NotificationPickedTableViewCell) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = (cell.notification?.user_id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func commentTapped(cell: NotificationPickedTableViewCell) {
        
        showActivityIndicator()
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        guard let picked_group_id = cell.notification?.picked_group_id else {return}
        guard let picked_id = cell.notification?.picked_id else {return}
        
        APIClient.getPickedGroup(user_id: userId, picked_group_id: picked_group_id) { (pickedGroup, error) in
            
            DispatchQueue.main.async {
                self.hideActivityIndicator()
            }
            
            if let err = error{
                print(err)
                self.showMessageTop(body: err)
                return
            }
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            let vc = PickedDetailedViewController(collectionViewLayout: layout)
            vc.isHeroEnabled = true
            vc.heroModalAnimationType = .selectBy(presenting: .cover(direction: .up), dismissing: .uncover(direction: .down))
            vc.pickedGroup = pickedGroup
            for (index,picked) in (pickedGroup.picked_group.enumerated()){
                if picked.id == picked_id{
                    let indexPath = IndexPath(row: index, section: 0)
                    vc.indexToScroll = indexPath
                }
            }
            vc.isCurrentUser = false
            //  The entire picked group exists put picked does not exists
            if vc.indexToScroll == nil{
                self.showMessageTop(body: "Picked does not exists".localized())
            }else{
               self.present(vc, animated: true, completion: nil)
            }
            
            print(pickedGroup)
        }
        
//        let tagId = cell.notificationCellData?.notification?.tagId
//        let pickedId = cell.notificationCellData?.notification?.pickedId
//        let userId = cell.notificationCellData?.notification?.fromUserId
//        let userProfile = cell.notificationCellData?.userProfile
//        showActivityIndicator()
//
//        Service.shared.getPickedGroup(userId: userId!, tagId: tagId!) { (pickedList, err) in
//
//            if let error = err{
//                // The entire picked group does not exists
//                self.hideActivityIndicator()
//                self.showMessageTop(body: error)
//            }else{
//                self.hideActivityIndicator()
//                let layout = UICollectionViewFlowLayout()
//                layout.scrollDirection = .horizontal
//                let vc = PickedDetailedViewController(collectionViewLayout: layout)
//                vc.isHeroEnabled = true
//                vc.heroModalAnimationType = .selectBy(presenting: .cover(direction: .up), dismissing: .uncover(direction: .down))
//                vc.pickedList = pickedList!
//                for (index,picked) in (pickedList?.enumerated())!{
//                    if picked.id == pickedId{
//                        let indexPath = IndexPath(row: index, section: 0)
//                        vc.indexToScroll = indexPath
//                    }
//                }
//                vc.isCurrentUser = false
//                vc.userProfile = userProfile!
//                //  The entire picked group exists put picked does not exists
//                if vc.indexToScroll == nil{
//                    self.showMessageTop(body: "Picked does not exists".localized())
//                }else{
//                   self.present(vc, animated: true, completion: nil)
//                }
//
//            }
//
//        }
    }
    
    
}





//extension NotificationViewController2: UIScrollViewDelegate{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == self.listTableView {
//            if scrollView.contentOffset.y < -110 {
//                scrollView.scrollRectToVisible(CGRect(origin: CGPoint(x: 0, y: -110), size: scrollView.frame.size), animated: false)
//                scrollView.scrollRectToVisible(CGRect(origin: CGPoint.zero, size: scrollView.frame.size), animated: true)
//            }
//        }
//    }
//}
