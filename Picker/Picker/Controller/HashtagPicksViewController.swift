//
//  HashtagPicksViewController.swift
//  Picker
//
//  Created by Omar mohammed on 7/22/19.
//  Copyright © 2019 Omar basaleh. All rights reserved.
//

import UIKit

class HashtagPicksViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pickedGroupsCollectionView: UICollectionView!
    var hashtag: String!
    var pickedGroups = [PickedGroup]()
    var refreshsControl = UIRefreshControl()
    
    init(hashtag: String) {
        self.hashtag = hashtag
        super.init(nibName: "HashtagPicksViewController", bundle: .main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle()
        registerCollectionViewCells()
        setNavigationBarButtons()
        setupRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    
    @objc fileprivate func fetchData(){
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.getHashtagPickedGroups(user_id: "\(userId)", hashtag: hashtag) { [unowned self] (pickedGroups, error) in
            DispatchQueue.main.async {
                self.refreshsControl.endRefreshing()
                self.activityIndicator.stopAnimating()
                self.pickedGroups = pickedGroups
                self.pickedGroupsCollectionView.reloadData()
            }
        }
    }
    
    fileprivate func setupRefreshControl() {
        pickedGroupsCollectionView.refreshControl = refreshsControl
        refreshsControl.addTarget(self, action: #selector(fetchData), for: .valueChanged)
    }
    
    @objc func dismissView(){
        removeHeroIhFromCellsBeforeDismiss()
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func removeHeroIhFromCellsBeforeDismiss(){
        for cell in pickedGroupsCollectionView.visibleCells as! [ProfilePickedCollectionViewCell]{
            cell.thumbImage.hero.id = ""
        }
    }
    
    fileprivate func setTitle(){
        title = hashtag
    }
    
    fileprivate func registerCollectionViewCells(){
        pickedGroupsCollectionView.register(SearchPickedCollectionViewCell.self, forCellWithReuseIdentifier: "CellId")
    }
    
    fileprivate func setNavigationBarButtons(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), style: .plain, target: self, action: #selector(dismissView))
    }
    
}

extension HashtagPicksViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pickedGroups.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellId", for: indexPath) as! SearchPickedCollectionViewCell
        
        cell.configure(pickedGroup: pickedGroups[indexPath.item])
        cell.user = pickedGroups[indexPath.item].user
        cell.thumbImage.layer.borderWidth = 0
        cell.profileImage.isUserInteractionEnabled = true
        let location = pickedGroups[indexPath.item].picked_group.first?.location
        cell.locationLabel.text = location
        cell.searchDelegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let vc = PickedDetailedViewController(collectionViewLayout: layout)
        vc.hero.isEnabled = true
        vc.pickedGroup = pickedGroups[indexPath.item]
        vc.indexPath = indexPath
        vc.collectionView?.reloadData()
        present(vc, animated: true, completion: nil)
        
    }
}


extension HashtagPicksViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 - 24, height: (view.frame.width/2 - 24) + 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
}

extension HashtagPicksViewController: SearchPickedCollectionViewCellDelegate{
    func profileSelected(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        vc.hero.isEnabled = true
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        self.present(vc, animated: true, completion: nil)
    }
}
