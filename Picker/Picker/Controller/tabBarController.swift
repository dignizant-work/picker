//
//  tabBarController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import KDCircularProgress

class appTabBarController: UITabBarController
{
    let  progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
    var fromDeepLink = false
    var deepLinkNotificationId: Int?
   
    convenience init(deepLinkId: Int?)
    {
        self.init()
        if let notificationNav = self.viewControllers![3] as? UINavigationController{
            if let notificationVC = notificationNav.viewControllers.first as? NotificationViewController{
                if let deepLinkId = deepLinkId{
                    notificationVC.loadDeepLink(deepLinkId: deepLinkId)
                }else{
                    notificationVC.loadData()
                }
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.groupTableViewBackground
        tabBar.barTintColor = UIColor.clear
        tabBar.tintColor = AppColors.shared.blue
        tabBar.unselectedItemTintColor = .white
        tabBar.isTranslucent = true
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = #imageLiteral(resourceName: "tab_overlay")
        
        //Set up progress
        progress.dropShadow()
        progress.isHidden = true
        progress.progressThickness = 0.3
        progress.trackThickness = 0.3
        progress.trackColor = .white
        progress.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cameraPressed)))
        progress.isUserInteractionEnabled = true
        progress.set(colors: UIColor.cyan ,UIColor.white, UIColor.magenta, UIColor.white, UIColor.orange)
        view.addSubview(progress)
        
        progress.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-32)
            } else {
                make.bottom.equalTo(view.snp.bottom).offset(-32)
            }
            make.width.height.equalTo(80)
        }
        
        let searchViewController = SearchViewController()
        searchViewController.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "ic_search_36pt"), tag: 0)
        
        let pickedViewController = HomeViewController()
        pickedViewController.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "Home"), tag: 1)
        
        let cameraViewControllers = CameraViewController()
        cameraViewControllers.tabBarItem = UITabBarItem(title: nil, image: nil, tag: 2)
        
        let notificationViewController = NotificationViewController()
        notificationViewController.title = "Notifications".localized()
        notificationViewController.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "Notitfication"), tag: 3)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        profileViewController.isTabBarProfile = true
        profileViewController.isCurrentUser = true
        profileViewController.title = "Profile".localized()
        profileViewController.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "Account"), tag: 4)
        
        
//        let settingViewController = SettingsViewController()
//        settingViewController.title = "Settings".localized()
//        settingViewController.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "setting"), tag: 3)

//        let viewControllerList = [ searchViewController, pickedViewController, cameraViewControllers, notificationViewController, settingViewController ]
        let viewControllerList = [ searchViewController, pickedViewController, cameraViewControllers, notificationViewController, profileViewController ]
        viewControllers = viewControllerList.map { UINavigationController(rootViewController: $0) }
        
        if let items = tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsets.init(top: 6, left: 0, bottom: -6, right: 0);
            }
        }
        self.tabBar.items?[2].isEnabled = false
        selectedIndex = 2
    }
   
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        tabBar.barTintColor = .white
        tabBar.unselectedItemTintColor = AppColors.shared.lightGray

        progress.removeShadow()
        progress.isHidden = false
        progress.trackColor = AppColors.shared.lightGray
        self.progress.snp.removeConstraints()
        self.progress.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            } else {
                make.bottom.equalTo(view.snp.bottom)
            }
            make.width.height.equalTo(50)
        }

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (true) in
            let cameraVC = self.viewControllers![2].children[0] as? CameraViewController
            cameraVC?.progress.isHidden = true
        }

        tabBar.isTranslucent = false
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
    }
    
    
    @objc func cameraPressed()
    {
        print("pressed")
        tabBar.barTintColor = .clear
        tabBar.unselectedItemTintColor = .white
        tabBar.isUserInteractionEnabled = false
        self.selectedIndex = 2
        
        progress.dropShadow()
        progress.trackColor = .white
        progress.snp.removeConstraints()
        progress.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(-32)
            } else {
                make.bottom.equalTo(view.snp.bottom).offset(-32)
            }
            make.width.height.equalTo(80)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
             self.view.layoutIfNeeded()
        }) { (true) in
            self.progress.isHidden = true
            self.tabBar.isUserInteractionEnabled = true
            let cameraVC = self.viewControllers![2].children[0] as? CameraViewController
            cameraVC?.progress.isHidden = false
        }
        
        tabBar.isTranslucent = true
        tabBar.backgroundImage = #imageLiteral(resourceName: "tab_overlay")
    }
    
    func selectNotificationTab(){
        
        selectedIndex = 3
        tabBar.barTintColor = .white
        tabBar.unselectedItemTintColor = AppColors.shared.lightGray
        
        progress.removeShadow()
        progress.isHidden = false
        progress.trackColor = AppColors.shared.lightGray
        self.progress.snp.removeConstraints()
        self.progress.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            } else {
                make.bottom.equalTo(view.snp.bottom)
            }
            make.width.height.equalTo(50)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (true) in
            let cameraVC = self.viewControllers![2].children[0] as? CameraViewController
            cameraVC?.progress.isHidden = true
        }
        
        tabBar.isTranslucent = false
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
    }
    
    
   
    
    
   


}
