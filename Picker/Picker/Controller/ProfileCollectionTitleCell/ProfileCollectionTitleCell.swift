//
//  ProfileCollectionTitleCell.swift
//  Picker
//
//  Created by Vishal on 27/04/20.
//  Copyright © 2020 Omar basaleh. All rights reserved.
//

import UIKit

class ProfileCollectionTitleCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var btnFollow: TWTButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnDraft: UIButton!
    
    @IBOutlet weak var btnFollowers: UIButton!
    @IBOutlet weak var btnFollowing: UIButton!
    @IBOutlet weak var followActivityIndicater: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

}
