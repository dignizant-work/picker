//
//  PrivacyPolicyViewController.swift
//  Picker
//
//  Created by Vishal on 27/04/20.
//  Copyright © 2020 Omar basaleh. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Privacy Policy".localized()
        
        let myWebView:WKWebView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-130))
        
        let urlstr = K.ProductionServer.baseURL + K.ProductionServer.privacy_policy
        print("URL: \(urlstr)")
        
        
        myWebView.load(NSURLRequest(url: NSURL(string: "http://picker.vavisa-kw.com/privacy_policy")! as URL) as URLRequest)
        self.view.addSubview(myWebView)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(handleCancel))
    }
    
    @objc func handleCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
