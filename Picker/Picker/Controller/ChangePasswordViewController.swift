//
//  ChangePasswordViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 5/26/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    let indecator = UIActivityIndicatorView()

    let oldPasswordTF:customTextField = {
        let tf = customTextField()
        tf.isSecureTextEntry = true
         tf.autocapitalizationType = .none
        tf.placeholder = "Old password".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_lock_outline_36pt"))
        return tf
    }()
    
    let newPasswordTF:customTextField = {
        let tf = customTextField()
        tf.isSecureTextEntry = true
        tf.autocapitalizationType = .none
        tf.placeholder = "New password".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_lock_outline_36pt"))
        return tf
    }()
    
    let confirmNewPasswordTF:customTextField = {
        let tf = customTextField()
        tf.isSecureTextEntry = true
        tf.autocapitalizationType = .none
        tf.placeholder = "Confirm Password".localized()
        tf.setLeftIcon(#imageLiteral(resourceName: "ic_lock_outline_36pt"))
        return tf
    }()
    
    let errorLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont(name: "Raleway-Medium", size: 12)
        label.textColor = AppColors.shared.red
        return label
    }()
    
    func setupViews()
    {
        self.title = "Change password".localized()
        view.backgroundColor = .white
        
        view.addSubview(oldPasswordTF)
        oldPasswordTF.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(24)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        view.addSubview(newPasswordTF)
        newPasswordTF.snp.makeConstraints { (make) in
            make.top.equalTo(oldPasswordTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        view.addSubview(confirmNewPasswordTF)
        confirmNewPasswordTF.snp.makeConstraints { (make) in
            make.top.equalTo(newPasswordTF.snp.bottom).offset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(view.frame.width/1.2)
            make.height.equalTo(35)
        }
        
        view.addSubview(errorLabel)
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(confirmNewPasswordTF.snp.bottom).offset(32)
//            make.top.equalTo(newPasswordTF.snp.bottom).offset(32)
            make.left.right.equalToSuperview().inset(8)
            
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(handleDone))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(handleCancel))
    }
    
    func showIndecator(){
        navigationItem.titleView = indecator
        indecator.startAnimating()
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.leftBarButtonItem?.isEnabled = false
    }
    
    func hideIndecator(){
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text =  "Change password".localized()
        navigationItem.titleView = label
    
        indecator.stopAnimating()
        
        navigationItem.rightBarButtonItem?.isEnabled = true
        navigationItem.leftBarButtonItem?.isEnabled = true
    }
    
    @objc func handleDone(){
        
        if self.newPasswordTF.text != self.confirmNewPasswordTF.text {
            self.errorLabel.text = "Your password and confirmation password do not match.".localized()
            return
        }
        
        showIndecator()
        guard let oldPassword = oldPasswordTF.text, let newPassword = newPasswordTF.text else {return}
        let user_id = UserDefaults.standard.integer(forKey: "userId")
        
        APIClient.changePassword(user_id: user_id, old_password: oldPassword, new_password: newPassword) { (error) in
            self.hideIndecator()
            if let err = error{
                self.errorLabel.text = err
                return
            }
            self.navigationController?.popViewController(animated: true)
        }

    }
    
    @objc func handleCancel(){
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }
}
