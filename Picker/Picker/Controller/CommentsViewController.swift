 //
//  CommentsViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 2/18/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import GrowingTextView
import IQKeyboardManagerSwift
import SwiftRichString
import Hero
import SnapKit

 struct ExpandableComment{
    var comment: Comment
    var isExpanded: Bool
 }
 
 
 enum CellType{
    case comment, like
 }

 protocol CommentsViewControllerDelegate{
    func increaseCommentsCountByOne(pickedId: Int)
    func decreaseCommentsCountByOne(pickedId: Int)
 }

class CommentsViewController: UIViewController {
    
    var pickedId: Int = 0
    var picked_group = PickedGroup()
    var commentIdToScrollTo:Int?
    var isNotificationReply = false
    var isCurrentUser: Bool = false
    var pageNumber:Int = 0
    private var comments:[Comment] = []
    private var likeList:[UserShort] = []
    private var lastIndexPath:IndexPath = IndexPath()
    private var commentIdForReply:String?
    private let refreshControl = UIRefreshControl()
    
    
    var mentionedUsers = [String]()
    var panGR: UIPanGestureRecognizer!
    var delegate: CommentsViewControllerDelegate?
    var cellType : CellType = .comment
    var isReply = false
    var replyCommentId = 0
    var tableViewBottomConstraint: Constraint!
    
    
    let isEmptyLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Bold", size: 18)
        label.textAlignment = .center
        label.text = "No comments.".localized()
        label.isHidden = true
        label.numberOfLines = 0
        return label
    }()
    
    var expandableComments = [ExpandableComment]()
    
    let downloadService = DownloadService()
    
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        downloadService.downloadsSession = downloadsSession
        
        navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil);
        
        panGR = UIPanGestureRecognizer(target: self,action: #selector(handlePan(gestureRecognizer:)))
        view.addGestureRecognizer(panGR)
        
        //tableView.register(CommentTableViewCell.self, forCellReuseIdentifier: cellId)
       
        setupTableView()
        setupRefrehser()
        loadComments()
    }
    
    
    func setupTableView(){
        tableView.register(CommentPickedHeaderView.self, forHeaderFooterViewReuseIdentifier: "CommentPickedHeaderView")
        tableView.register(CommentHeaderViewCell.self, forHeaderFooterViewReuseIdentifier: "CommentHeaderViewCell")
        tableView.register(ReplyTableViewCell.self, forCellReuseIdentifier: "ReplyTableViewCell")
        tableView.register(BlockedUserTableViewCell.self, forCellReuseIdentifier: "BlockedUserTableViewCell")
        tableView.separatorColor = .clear
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.keyboardDismissMode = .onDrag
    }
    
    
    func downloadPicked(){
        // Download picked if its not downloaded
        for picked in picked_group.picked_group{
            if picked.downloadedUrl == nil{
                self.downloadService.startDownload(picked)
            }
        }
    }
    
    
    func handleEmptyLikesOrComments() {
        switch cellType {
        case .like:
            isEmptyLabel.text = "No likes.".localized()
            if likeList.isEmpty{
                isEmptyLabel.isHidden = false
            }else{
                isEmptyLabel.isHidden = true
            }
        case .comment:
            isEmptyLabel.text = "No comments.".localized()
            if comments.isEmpty{
                isEmptyLabel.isHidden = false
            }else{
                isEmptyLabel.isHidden = true
            }
        }
    }
    
    func loadComments(){
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.getLikesList(picked_id: pickedId, user_id: userId) { (usersList, error) in
            if let err = error{
                print(err)
                return
            }
            
            self.likeList.removeAll()
            self.likeList.append(contentsOf: usersList)
            self.handleEmptyLikesOrComments()

        }
        
        APIClient.getPicked(user_id: userId, picked_id: pickedId) { (picked_group, error) in

            if let err = error{
                print(err)
                return
            }

            self.picked_group = picked_group
            self.downloadPicked()
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            APIClient.getPickedComments(user_id: userId, picked_id: self.pickedId) { (comments, error) in
                if error != nil {
                    self.indecator.stopAnimating()
                    
                    let label = UILabel()
                    label.text = error
                    label.font = UIFont(name: "raleway-semibold", size: 18)
                    label.textColor = AppColors.shared.gray
                    self.view.addSubview(label)
                    self.commentTV.isHidden = true
                    self.sendBtn.isHidden = true
                    label.snp.makeConstraints({ (make) in
                        make.center.equalToSuperview()
                    })
                    
                }
                    
                else{
                    
                    self.expandableComments.removeAll()
                    self.comments = comments
                    for comment in comments{
                        self.expandableComments.append(ExpandableComment(comment: comment, isExpanded: false))
                    }
                    
                    self.handleEmptyLikesOrComments()
                    
                    if comments.isEmpty{
                        self.indecator.stopAnimating()
                    }else{
                        self.tableView.reloadData()
                        self.indecator.stopAnimating()
                        if self.commentIdToScrollTo != nil{
                            self.scrollToComment()
                        }else{
                            self.scrollToBottom()
                        }
                        
                        if self.isNotificationReply{
                            self.showReplies()
                        }
                    }
                    
                    if self.refreshControl.isRefreshing{
                        self.refreshControl.endRefreshing()
                    }
                    
                }
            }
            
            
        }
        
        
        
    }
    
    func setupRefrehser(){
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    
    @objc func handleRefresh(){
        loadComments()
    }
    
    func showReplies(){
        if let id = commentIdToScrollTo{
            for (index,expandableComment) in expandableComments.enumerated(){
                if expandableComment.comment.id == id{
                    openRepliesFor(section: index)
                }
            }
        }
        
    }
    
    
    func openRepliesFor(section: Int){
       
        // Riverse isExpanded
        expandableComments[section].isExpanded = !expandableComments[section].isExpanded
        
        
        var indexPaths = [IndexPath]()
        for i in 0..<expandableComments[section].comment.replies.count{
            indexPaths.append(IndexPath(row: i, section: section+1))
        }
        
        
        if expandableComments[section].isExpanded{
            self.tableView.insertRows(at: indexPaths, with: .automatic)
            
//            if let label = sender.view as? UILabel {
//                label.text = "-" + "Hide replies".localized()
//            }
        }else{
            tableView.deleteRows(at: indexPaths, with: .none)
//            if let label = sender.view as? UILabel {
//                label.text = "-" + "View replies".localized() + " " + "(" + String(expandableComments[section-1].comment.replies.count) + ")"
//            }
        }
    }
 
    
    @objc func hideKeyboard(){
        commentTV.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        
        guard let keyboardFrame = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        var keyboardHeight: CGFloat
        
        
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2688:
                // iPhone Xs Max
                keyboardHeight += 25
            case 1792:
                // iPhone Xr
                keyboardHeight += 25
            default:
                print("unknown")
            }
        }
        
        contentView.snp.removeConstraints()
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(keyboardHeight)
        }
        
        
//        commentTV.snp.removeConstraints()
//        commentTV.snp.makeConstraints { (make) in
//            make.leading.trailing.equalToSuperview()
//            make.bottom.equalToSuperview().inset(keyboardHeight)
//        }
//
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        if !isReply{
            if comments.count > 0{
                scrollToBottom()
            }
        }
        
        
        
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        
        contentView.snp.removeConstraints()
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
    }
    
    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    let viewTitle:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text = "Comments".localized()
        return label
    }()
    
    lazy var sendBtn:UIButton = {
        let btn =  BlueShadowButton()
        btn.isEnabled = false
        btn.alpha = 0.5
        btn.setTitle("Send".localized(), for: .normal)
        btn.addTarget(self, action: #selector(sendComment), for: .touchUpInside)
        return btn
    }()
    
    lazy var backBtn:UIButton = {
        let btn = UIButton()
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_right_36pt"), for: .normal)
        }else{
            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), for: .normal)
        }
        btn.tintColor = .white
        btn.addTargetClosure(closure: { (btn) in
            self.dismiss(animated: true, completion: nil)
        })
        return btn
    }()
    
    let CommentTVBackgroundView:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.white
        return v
    }()
    
    let isReplyingView:UIView = {
        let v = UIView()
        v.isHidden = true
        v.backgroundColor = UIColor.groupTableViewBackground
        return v
    }()
    
    let isReplyingLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 13)
        label.textColor = AppColors.shared.gray
        return label
    }()
    
    lazy var cancelReplyBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "ic_close_36pt"), for: .normal)
        btn.tintColor = AppColors.shared.gray
        btn.addTarget(self, action: #selector(handleCancelReply), for: .touchUpInside)
        return btn
    }()
    
    @objc func handleCancelReply(){
        self.isReply = false
        self.commentTV.text = ""
        commentTVCounter.text = ""
        isReplyingView.isHidden = true
        sendBtn.alpha = 0.5
        sendBtn.isEnabled = false
        
    }
    
    
    
    lazy var commentTV:GrowingTextView = {
        let tv = GrowingTextView()
        tv.placeholder = "Add comment".localized()
        tv.font = UIFont(name: "Raleway-Medium", size: 17)
        tv.backgroundColor = .white
        tv.textColor = AppColors.shared.darkGray
        tv.layer.borderColor = AppColors.shared.superLightGray.cgColor
        tv.layer.borderWidth = 1
        tv.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        tv.textContainer.maximumNumberOfLines = 6
        tv.textContainer.lineBreakMode = .byTruncatingTail
        tv.maxLength = 140
        return tv
    }()
    
    let commentTVCounter:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 17)
        label.textColor = AppColors.shared.blue
        return label
    }()
    
    lazy var tableView:UITableView = {
        let tv = UITableView(frame:CGRect.zero, style: UITableView.Style.grouped)
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
        tv.backgroundColor = .white
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()
    
    var contentView:UIView = {
        let v = UIView()
        return v
    }()
    
    
    
    let indecator:UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        i.style = .gray
        i.startAnimating()
        return i
    }()
    
    
    @objc func sendComment(){
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        if isReply{
            
            self.isReplyingView.isHidden = true
            self.isReply = false
            
            APIClient.addReply(user_id: userId, comment_id: replyCommentId, message: commentTV.text, mentioned_usernames: mentionedUsers) { (reply, error) in
                
                if let err = error{
                    print(err)
                    return
                }
                let index = self.expandableComments.firstIndex(where: { (expandableComment) -> Bool in
                    expandableComment.comment.id == self.replyCommentId
                })
                
                if let section = index{
                    self.comments[section].replies.append(reply)
                    if self.expandableComments[section].isExpanded {
                        self.expandableComments[section].comment.replies.append(reply)
                        self.tableView.insertRows(at: [IndexPath(row: self.expandableComments[section].comment.replies.count-1, section: section+1)], with: .none)
                    }else{
                        // Expand replies for the comment
                        self.expandableComments[section].comment.replies.append(reply)
                        if let sectionHeader = self.tableView.headerView(forSection: section+1) as? CommentHeaderViewCell{
                            sectionHeader.viewRepliesLabel.text = "-" + "Hide replies".localized()
                        }
                        self.openRepliesFor(section: section)

                    }
                    
                }
                
            }
        }else{
            print("PARAM:- user_id:\(userId),picked_id:\(pickedId),message:\(commentTV.text!),mentioned_usernames:\(mentionedUsers)")
            APIClient.addComment(user_id: userId, picked_id: pickedId, message: commentTV.text, mentioned_usernames: mentionedUsers) { (comment, error) in
                
                if let err = error{
                    print(err)
                    return
                }
                
                self.delegate?.increaseCommentsCountByOne(pickedId: self.pickedId)
                
                self.expandableComments.insert(ExpandableComment(comment: comment, isExpanded: false), at: self.expandableComments.count)
                self.comments.insert(comment, at: self.comments.count)
                self.isEmptyLabel.isHidden = false
                self.tableView.insertSections(IndexSet.init(integer: self.expandableComments.count), with: .none)
                self.scrollToBottom()
            }
        }
        
        
        
        commentTV.text = ""
        commentTVCounter.text = ""
        commentTV.resignFirstResponder()
        sendBtn.alpha = 0.5
        sendBtn.isEnabled = false
        
        

        
    }
    
    func scrollToComment()
    {
        /*
         * Sroll to spicific comment if commentIdToScrollTo != nil, else scroll to bottom
         */
        var section:Int = Int()
        if let id = commentIdToScrollTo{
            for (index,expandableComment) in expandableComments.enumerated(){
                if expandableComment.comment.id == id{
                    section = index
                }
            }
        }else{
            section = self.expandableComments.count
        }
        
        let indexPath = IndexPath(row: NSNotFound, section:section+1)
        
        DispatchQueue.main.async {
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
            // Highlight cell if it is visible
            if let sectionHeader = self.tableView.headerView(forSection: section+1) as? CommentHeaderViewCell{
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    sectionHeader.contentView.backgroundColor = UIColor(white: 0.95, alpha: 1)
                }, completion: nil)
            }
        }
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        /*
         * Highlight spicific comment if commentIdToScrollTo != nil and cell is not visible
         */
        var section:Int = Int()
        if let id = commentIdToScrollTo{
            for (index,expandableComment) in expandableComments.enumerated(){
                if expandableComment.comment.id == id{
                    section = index
                }
            }
            
            DispatchQueue.main.async {
                if let sectionHeader = self.tableView.headerView(forSection: section+1) as? CommentHeaderViewCell{
                    UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        sectionHeader.contentView.backgroundColor = UIColor(white: 0.95, alpha: 1)
                    }, completion: nil)
                }


            }
        }
        
        
        
        
    }
    
    
    func scrollToBottom(){
        
        if cellType == .comment{
            DispatchQueue.main.async {
                let indexPath = IndexPath.init(row: NSNotFound, section: self.expandableComments.count)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
        
        
    }
    
    @objc func handleCancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    // Hero dismiss interactive animation
    @objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
        let translation = panGR.translation(in: nil)
        var progress = CGFloat()
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            progress = -translation.x / view.bounds.width
        }else{
            progress = translation.x / view.bounds.width
        }
        
        
        switch panGR.state {
            
        case .began:
            dismiss(animated: true, completion: nil)
            
        case .changed:
            // calculate the progress based on how far the user moved
            Hero.shared.update(CGFloat(progress))
        default:
            // end the transition when user ended their touch
            if progress  > 0.25 {
                Hero.shared.finish()
            }else{
                Hero.shared.cancel()
            }
        }
    }
    
    
    func setupViews(){
  
        view.backgroundColor = .white
        
        view.addSubview(blueView)
        blueView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    //iPhone X, XS
                    make.height.equalTo(88)
                    
                case 2688:
                    //iPhone XS Max
                    make.height.equalTo(88)
                    
                case 1792:
                    //iPhone XR
                    make.height.equalTo(88)
                    
                default:
                    make.height.equalTo(64)
                }
            }
        }

        blueView.addSubview(backBtn)
        backBtn.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(8)
            make.height.width.equalTo(35)
        }

        blueView.addSubview(viewTitle)
        viewTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(14)
        }
        
        view.insertSubview(contentView, at: 0)
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(blueView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        
        
        
        contentView.addSubview(sendBtn)
        sendBtn.snp.makeConstraints { (make) in

            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    // iPhoneX
                    make.bottom.equalToSuperview().offset(-36)
                default:
                    make.bottom.equalToSuperview().offset(-16)
                }
            }
            
            make.trailing.equalToSuperview().inset(8)
            make.width.equalTo(75)
            make.height.equalTo(40)
        }
        
        contentView.addSubview(commentTV)
        commentTV.snp.makeConstraints { (make) in
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 2436:
                    // iPhoneX
                    make.bottom.equalToSuperview().offset(-36)
                default:
                    make.bottom.equalToSuperview().offset(-16)
                }
            }
            make.leading.equalToSuperview().inset(8)
            make.trailing.equalTo(sendBtn.snp.leading).inset(-8)
        }
        commentTV.sizeThatFits(CGSize(width: commentTV.frame.size.width, height: commentTV.frame.size.height))
        commentTV.layer.cornerRadius = 20
        commentTV.delegate = self
        
        
        contentView.insertSubview(CommentTVBackgroundView, at: 0)
        CommentTVBackgroundView.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(commentTV).offset(-24)
        }
        
        contentView.addSubview(commentTVCounter)
        commentTVCounter.snp.makeConstraints { (make) in
            make.left.equalTo(commentTV).inset(8)
            make.bottom.equalTo(commentTV.snp.top).offset(-4)
        }
        
        
        
        contentView.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(CommentTVBackgroundView.snp.top)
        }
       
        
        contentView.addSubview(isReplyingView)
        isReplyingView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(CommentTVBackgroundView.snp.top)
            make.height.equalTo(44)
        }
        
        isReplyingView.addSubview(cancelReplyBtn)
        cancelReplyBtn.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-16)
            make.height.width.equalTo(15)
        }
       
        isReplyingView.addSubview(isReplyingLabel)
        isReplyingLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalTo(cancelReplyBtn.snp.leading).offset(-16)
        }
        
        contentView.addSubview(indecator)
        indecator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.width.equalTo(35)
        }
        
        tableView.addSubview(isEmptyLabel)
        isEmptyLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(100)
            make.leading.trailing.equalToSuperview().inset(8)
        }
        
    }
    
}


extension CommentsViewController:UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch cellType{
        case .comment:
            
            if section == 0{
                return 0
            }else{
                if expandableComments[section-1].isExpanded{
                    return expandableComments[section-1].comment.replies.count
                }else{
                    return  0
                }
            }
            
            
        case .like:
            return likeList.count
        }
        
       
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section != 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReplyTableViewCell", for: indexPath) as! ReplyTableViewCell
            cell.reply = expandableComments[indexPath.section-1].comment.replies[indexPath.row]
            cell.comment = expandableComments[indexPath.section-1].comment
            
            // Hide more button from unauthorized users
            let userId = UserDefaults.standard.integer(forKey: "userId")
            
            if expandableComments[indexPath.section-1].comment.replies[indexPath.row].user_id == userId{
                cell.moreBtn.isHidden = false
            }else{
                cell.moreBtn.isHidden = true
            }
            
            //        if isCurrentUser{
            //             cell.moreBtn.isHidden = false
            //        }else{
            //
            //        }
            
            cell.delegate = self
            return cell
        }
        
        
        if cellType == .like{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlockedUserTableViewCell", for: indexPath) as! BlockedUserTableViewCell
            cell.userProfile = likeList[indexPath.row]
            return cell
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cellType == .like{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
            vc.profile_user_id = likeList[indexPath.row].id
            vc.hero.isEnabled = true
            
            if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
            }else{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cellType {
        case .comment:
            return UITableView.automaticDimension
        case .like:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            if isCurrentUser{
                return 300 + 50
            }
            return 300
        }
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch cellType {
        case .comment:
            return expandableComments.count + 1
        case .like:
            return 1
        }
        
    }
    
     
    
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            // First section for picked header
            
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentPickedHeaderView" ) as? CommentPickedHeaderView
            header?.picked_group = picked_group
            header?.delegate = self
            
            if !isCurrentUser{
                header?.viewCountLabel.isHidden = true
                header?.viewCountImageView.isHidden = true
                header?.tabsStackView.isHidden = true
                header?.horizontalLine2.isHidden = true
            }
            return header
        }
        else{
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentHeaderViewCell" ) as? CommentHeaderViewCell
            header?.comment = expandableComments[section-1].comment
            header?.delegate = self
            header?.viewRepliesLabel.tag = section
            header?.viewRepliesLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOpenCloseComment)))
            
            if expandableComments[section-1].comment.replies.count > 0{
                if expandableComments[section-1].isExpanded{
                    header?.viewRepliesLabel.text = "-" + "Hide replies".localized()
                }else{
                    header?.viewRepliesLabel.text = "-" + "View replies".localized() + " " + "(" + String(expandableComments[section-1].comment.replies.count) + ")"
                }
            }
            
            header?.backgroundColor = .white
            
            let userId = UserDefaults.standard.integer(forKey: "userId")
            
            if expandableComments[section-1].comment.user_id == userId{
                header?.moreBtn.isHidden = false
            }else{
                header?.moreBtn.isHidden = true
            }
            
            return header
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.000001
    }
    
    @objc func handleOpenCloseComment(sender: UITapGestureRecognizer){
        
        var section = 0
        if let label = sender.view as? UILabel {
            section = label.tag
        }
        
        
        // Riverse isExpanded
        expandableComments[section-1].isExpanded = !expandableComments[section-1].isExpanded
        
        
        var indexPaths = [IndexPath]()
        for i in 0..<expandableComments[section-1].comment.replies.count{
            indexPaths.append(IndexPath(row: i, section: section))
        }
        
        
        if expandableComments[section-1].isExpanded{
            self.tableView.insertRows(at: indexPaths, with: .automatic)
           
            if let label = sender.view as? UILabel {
                label.text = "-" + "Hide replies".localized()
            }
        }else{
            tableView.deleteRows(at: indexPaths, with: .none)
            if let label = sender.view as? UILabel {
                label.text = "-" + "View replies".localized() + " " + "(" + String(expandableComments[section-1].comment.replies.count) + ")"
            }
        }
        
    }
    
    
    
}

extension CommentsViewController:UITextViewDelegate{
    
    /**
     * Limit comment number of lines
     */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let existingLines = textView.text.components(separatedBy: CharacterSet.newlines)
        let newLines = text.components(separatedBy: CharacterSet.newlines)
        let linesAfterChange = existingLines.count + newLines.count - 1
        return linesAfterChange <= textView.textContainer.maximumNumberOfLines
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.count != 0 {
            commentTVCounter.text = String(textView.text.count)
        }else{
            commentTVCounter.text = ""
        }
        
        if textView.text.count == 0{
            sendBtn.alpha = 0.5
            sendBtn.isEnabled = false
        }else{
            
            print(textView.text ?? "")
            mentionedUsers = []
            for word in textView.text.components(separatedBy: " "){
                if let range = word.range(of: "@") {
                    let username = word[range.upperBound...]
                    mentionedUsers.append(String(username))
                }
            }
            sendBtn.isEnabled = true
            sendBtn.alpha = 1
        }
    }
    
//    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
//        return false
//    }
//
    
}


extension CommentsViewController:CommentHeaderViewCellDelegate{
    
    func profilePresses(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        vc.hero.isEnabled = true
        
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func morePressed(comment: Comment, cell: CommentHeaderViewCell) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive) { (_) in
            APIClient.deleteComment(comment_id: comment.id, completion: { (error) in
                
                if let err = error{
                    print(err)
                    return
                }
            })
            
            self.delegate?.decreaseCommentsCountByOne(pickedId: self.pickedId)
            let sectionIndex = self.expandableComments.firstIndex(where: { (expandableComment) -> Bool in
                expandableComment.comment.id == cell.comment.id
            })
            
            if let sectionIndex = sectionIndex{
                self.expandableComments.remove(at: sectionIndex)
                self.comments.remove(at: sectionIndex)
                self.tableView.deleteSections([sectionIndex+1], with: .automatic)
                self.handleEmptyLikesOrComments()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)
        
        // Only Picked owner and comment owner can delete the comment
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        if comment.user_id == userId{
            alertController.addAction(deleteAction)
        }else{
            
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func replyPressed(comment: Comment) {
        commentTV.becomeFirstResponder()
        commentTV.text = "@" + comment.username + " "
        isReply = true
        replyCommentId = comment.id
        
        isReplyingView.isHidden = false
        isReplyingLabel.text = "Replying to".localized() + " " + comment.username
    }
    
    func mentionPressed(username: String) {
        
        APIClient.getUserIdFromUsername(username: username) { (userId, error) in
            if let err = error{
                self.showMessageTop(body: err)
                return
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
            vc.profile_user_id = userId
            vc.hero.isEnabled = true
            
            if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
            }else{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
}
 
 extension CommentsViewController:ReplyTableViewCellDelegate{
    
    func morePressed(reply: Reply, cell: ReplyTableViewCell) {
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive) { (_) in
            APIClient.deleteReply(reply_id: cell.reply.id, completion: { (error) in
                if let err = error{
                    print(err)
                    return
                }
            })
            
            let sectionIndex = self.expandableComments.firstIndex(where: { (expandableComment) -> Bool in
                expandableComment.comment.id == cell.comment.id
            })
            
            
            if let sectionIndex = sectionIndex{
                if let index = cell.indexPath?.row{
                    self.expandableComments[sectionIndex].comment.replies.remove(at: index)
                    self.tableView.deleteRows(at: [IndexPath(row: index, section: sectionIndex+1)], with: .automatic)
                }
                
                if self.expandableComments[sectionIndex].comment.replies.count == 0{
                    if let sectionHeader = self.tableView.headerView(forSection: sectionIndex+1) as? CommentHeaderViewCell{
                        sectionHeader.viewRepliesLabel.text = ""
                    }
                }
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)
        
        // Only Picked owner and comment owner can delete the comment
//        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        if comment.user_id == userId{
//            
//        }else{
//            
//        }
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func replyProfilePresses(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        vc.hero.isEnabled = true
        
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func replyMentionPressed(username: String) {
        
        APIClient.getUserIdFromUsername(username: username) { (userId, error) in
            if let err = error{
                self.showMessageTop(body: err)
                return
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
            vc.profile_user_id = userId
            vc.hero.isEnabled = true
            
            if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
            }else{
                vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func replyToReplyPressed(reply: Reply, comment: Comment) {
        
        commentTV.becomeFirstResponder()
        commentTV.text = "@" + reply.username + " "
//        if reply.username == comment.username{
//           commentTV.text = "@" + comment.username + " "
//        }else{
//           commentTV.text = "@" + comment.username + " " + "@" + reply.username + " "
//        }
        
        isReply = true
        replyCommentId = comment.id
        
        isReplyingView.isHidden = false
        isReplyingLabel.text = "Replying to".localized() + " " + reply.username
        
    }
    
    
 }
 
 
 extension CommentsViewController: CommentPickedHeaderViewDelegate{
    func showComments() {
        cellType = .comment
        commentTV.isHidden = false
        sendBtn.isHidden = false
        
        tableView.snp.remakeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalTo(CommentTVBackgroundView.snp.top)
        }
        
        isEmptyLabel.text = "No comments.".localized()
        if comments.isEmpty{
            isEmptyLabel.isHidden = false
        }else{
            isEmptyLabel.isHidden = true
        }
        
        for comment in comments{
            self.expandableComments.append(ExpandableComment(comment: comment, isExpanded: false))
        }
        self.tableView.reloadData()
    }
    
    func showLikes() {
        cellType = .like
        commentTV.isHidden = true
        sendBtn.isHidden = true
        
        tableView.snp.remakeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        isEmptyLabel.text = "No likes.".localized()
        if likeList.isEmpty{
            isEmptyLabel.isHidden = false
        }else{
            isEmptyLabel.isHidden = true
        }
        
        expandableComments.removeAll()
        self.tableView.reloadData()
    }
    
    func pickedTapped(picked_group: PickedGroup)
    {
        let layout = UICollectionViewFlowLayout()
        let vc = PickedDetailedViewController(collectionViewLayout: layout)
        vc.isCurrentUser = isCurrentUser
        vc.pickedGroup = picked_group
        vc.hero.isEnabled = true
        vc.hero.modalAnimationType = .selectBy(presenting: .slide(direction: .up), dismissing: .slide(direction: .down))
        vc.isFromComments = true
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
 }
 
 extension CommentsViewController :URLSessionDownloadDelegate{
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
       
            for j in 0..<picked_group.picked_group.count{
                if picked_group.picked_group[j].url == sourceURL.absoluteString{
                    let destinationURL = localFilePath(for: sourceURL)
                    print(destinationURL)
                    // 3
                    let fileManager = FileManager.default
                    try? fileManager.removeItem(at: destinationURL)
                    do {
                        try fileManager.copyItem(at: location, to: destinationURL)
                    } catch let error {
                        print("Could not copy file to disk: \(error.localizedDescription)")
                    }
                    picked_group.picked_group[j].downloadedUrl = destinationURL
                    
                    DispatchQueue.main.async {
                        if let header = self.tableView.headerView(forSection: 0) as? CommentPickedHeaderView{
                            header.picked_group.picked_group[j].downloadedUrl = destinationURL
                        }
                    }
                   
                }
            }
    
        print("finished downloading")
        print(location)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print(Float(totalBytesWritten) / Float(totalBytesExpectedToWrite) * 100)
    }
    
 }



