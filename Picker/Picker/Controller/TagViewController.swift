//
//  TagViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/6/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import SnapKit
import TagListView

protocol TagViewControllerDelegate {
    func tagSelected(tag:String)
}

class TagViewController: UIViewController, TagListViewDelegate, UITextFieldDelegate  {

    var location = String()
    var tagDelegate:TagViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        APIClient.getUserTags(user_id: userId, location: location) { (userTags, error) in
            
            if let err = error{
                print(err)
                return
            }
            
            self.myTags.addTags(userTags.user_tags)
            self.NearbyTags.addTags(userTags.location_tags)
        }
        
        
//        Service.shared.getUserTags { (tagList) in
//            if let list = tagList{
//                for tag in list{
//                    self.myTags.addTag(tag)
//                }
//            }
//        }
//
//        Service.shared.getLocationTags(location: location) { (tagList) in
//            if let list = tagList{
//                for tag in list{
//                    self.NearbyTags.addTag(tag)
//                }
//            }
//        }
    }
    
    lazy var addTagTF:customTextField = {
        let tf = customTextField()
        tf.delegate = self
        tf.placeholder = "New tag".localized()
        tf.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        tf.setLeftIcon(#imageLiteral(resourceName: "Tag"))
        return tf
    }()
    
    let addTagBtn:BlueShadowButton = {
        let btn = BlueShadowButton()
        btn.setTitle("Add".localized(), for: .normal)
        btn.addTarget(self, action: #selector(addTag), for: .touchUpInside)
        btn.alpha = 0.5
        btn.isEnabled = false
        return btn
    }()
    
    let addTagBtnIndicator:UIActivityIndicatorView = {
        let v = UIActivityIndicatorView()
        return v
    }()
    
    let nearbyLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = AppColors.shared.gray
        label.text = "NEARBY".localized()
        return label
    }()
    
    let NearbyTags:TagListView = {
        let v = TagListView()
        v.borderColor = AppColors.shared.blue
        v.borderWidth = 1.2
        v.tagBackgroundColor = .clear
        v.textColor = AppColors.shared.blue
        v.textFont = UIFont(name: "Raleway-Semibold", size: 18)!
        v.cornerRadius = 20
        v.paddingX = 15
        v.paddingY = 10
        v.marginX = 10
        v.marginY = 15
        return v
    }()
    
    let myTagsLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = AppColors.shared.gray
        label.text = "MY TAGS".localized()
        return label
    }()
    
    let myTags:TagListView = {
        let v = TagListView()
        v.borderColor = AppColors.shared.blue
        v.borderWidth = 1.2
        v.tagBackgroundColor = .clear
        v.textColor = AppColors.shared.blue
        v.textFont = UIFont(name: "Raleway-Semibold", size: 18)!
        v.cornerRadius = 20
        v.paddingX = 15
        v.paddingY = 10
        v.marginX = 10
        v.marginY = 15
        return v
    }()
    
    func setupViews(){
        title = "Tag".localized()
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(handleCancel))
        
        view.addSubview(addTagBtn)
        addTagBtn.snp.makeConstraints { (make) in
            make.height.equalTo(35)
            make.width.equalTo(70)
            make.top.equalToSuperview().inset(32)
            make.right.equalToSuperview().inset(16)
        }
        
        addTagBtn.addSubview(addTagBtnIndicator)
        addTagBtnIndicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(addTagTF)
        addTagTF.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(32)
            make.left.equalToSuperview().offset(16)
            make.height.equalTo(35)
            make.right.equalTo(addTagBtn.snp.left).offset(-8)
        }
        
        view.addSubview(nearbyLabel)
        nearbyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(addTagTF.snp.bottom).offset(32)
            make.left.right.equalToSuperview().inset(16)
        }
        
        view.addSubview(NearbyTags)
        NearbyTags.snp.makeConstraints { (make) in
            make.top.equalTo(nearbyLabel.snp.bottom).offset(16)
            make.left.right.equalToSuperview().inset(16)
        }
        NearbyTags.delegate = self
        
        view.addSubview(myTagsLabel)
        myTagsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(NearbyTags.snp.bottom).offset(32)
            make.left.right.equalToSuperview().inset(16)
        }
        
        view.addSubview(myTags)
        myTags.snp.makeConstraints { (make) in
            make.top.equalTo(myTagsLabel.snp.bottom).offset(16)
            make.left.right.equalToSuperview().inset(16)
        }
        myTags.delegate = self
        
        
        
    }
    
    //MARK: taglist delegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print(title)
        if tagDelegate != nil{
            tagDelegate?.tagSelected(tag: title)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func handleCancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addTag(){
        guard let tag = addTagTF.text else {return}
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        APIClient.addTag(user_id: userId, name: tag, location: location) { (error) in
            
            if let err = error{
                print(err)
                self.dismiss(animated: true, completion: nil)
            }else{
                self.tagDelegate?.tagSelected(tag: tag)
                self.dismiss(animated: true, completion: nil)
            }
            
            
        }
        
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        if textField.text == "" {
            addTagBtn.alpha = 0.5
            addTagBtn.isEnabled = false
        }else{
            addTagBtn.alpha = 1
            addTagBtn.isEnabled = true
        }
    }
    
    //*************************************
    // Define max length for text field
    //*************************************
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 30
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    
    
   
}
