//
//  PeeksViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    private let pickedTimelineCellId = "pickedTimelineCellId"
    private var usersList = [UserProfile2]()
    private var timelineList = [Timeline]()
    private var isRefreshing  = false
    
    let downloadService = DownloadService()
    
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    var refresher:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadService.downloadsSession = downloadsSession
        
        setupViews()
        setupRefrehser()
        indicator.startAnimating()
        loadPicked()
        collectionView.register(TimelineCollectionViewCell.self, forCellWithReuseIdentifier: pickedTimelineCellId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
    }
    
    
    let indicator:UIActivityIndicatorView = {
        let i = UIActivityIndicatorView()
        i.activityIndicatorViewStyle = .gray
        return i
    }()
    
    lazy var collectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
        c.delegate = self
        c.dataSource = self
        c.backgroundColor = .white
        c.showsVerticalScrollIndicator = false
        return c
    }()
    
    
    let noPickedLabel:UILabel = {
        let label = UILabel()
        label.isHidden = true
        label.text = "No picked for today".localized()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    let viewTitle:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text = "Picked".localized()
        return label
    }()
    
    
    func setupRefrehser(){
        refresher = UIRefreshControl()
        refresher.tintColor = .lightGray
        refresher.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView.refreshControl = refresher
    }
    
    func downloadPickedGroups(){
        // Download picked if its not downloaded
        for timeLine in timelineList{
            for pickedGroup in timeLine.picked_groups{
                for picked in pickedGroup.picked_group{
                    if picked.downloadedUrl == nil{
                        self.downloadService.startDownload(picked)
                    }
                }
            }
        }
        
    }
   
    @objc func handleRefresh(){
        
        if !Reachability.isConnectedToNetwork(){
            showMessageTop(body: "No internet connection".localized())
            self.refresher.endRefreshing()
        }
        
        if !isRefreshing{
            loadPicked()
            isRefreshing = true
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        if refresher.isRefreshing{
            
        }
    }
    
    
    
    //MARK:- collection view methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return usersList.count
        return timelineList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pickedTimelineCellId, for: indexPath) as! TimelineCollectionViewCell
        //cell.userProfile = usersList[indexPath.item]
        cell.timeline = timelineList[indexPath.item]
        cell.pickedDelegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }

    
    func loadPicked(){
        let userId = UserDefaults.standard.integer(forKey: "userId")
        
        if !Reachability.isConnectedToNetwork(){
            showMessageTop(body: "No internet connection".localized())
            self.indicator.stopAnimating()
        }
        
        APIClient.getUserTimeline(user_id: userId) { (timelines, error) in
            
            self.indicator.stopAnimating()
            
            if let err = error{
                print(err)
                return
            }
            
            self.timelineList.removeAll()
            self.timelineList.append(contentsOf: timelines)
            self.downloadPickedGroups()
            self.isRefreshing = false
            self.refresher.endRefreshing()
            
            if timelines.count == 0{
                self.noPickedLabel.isHidden = false
            }else{
                self.noPickedLabel.isHidden = true
            }
            
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            
            print(timelines)
        }
        
        
    }
    
    func setupViews(){
        
        view.addSubview(blueView)
        blueView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            if UIDevice().userInterfaceIdiom == .phone {
                if  UIScreen.main.nativeBounds.height ==  2436{
                    make.height.equalTo(88)
                }else{
                    make.height.equalTo(64)
                }
            }
        }
        
        blueView.addSubview(viewTitle)
        viewTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(12)
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.top.equalTo(blueView.snp.bottom)
        }
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(noPickedLabel)
        noPickedLabel.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
}




extension HomeViewController: PickedTimelineDelegate{
    func profileSelected(userId: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pickedGroupSelected(pickedGroup: PickedGroup, indexPath: IndexPath) {
        print(pickedGroup)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let vc = PickedDetailedViewController(collectionViewLayout: layout)
        vc.isHeroEnabled = true
        vc.delegate = self
        vc.indexPath = indexPath

        vc.pickedGroup = pickedGroup

        
        vc.collectionView?.reloadData()
        present(vc, animated: true, completion: nil)
    }
}


extension HomeViewController: PickedDetailedViewControllerDelegate{
    
    func reload(index: IndexPath, userProfile: UserProfile2?) {
        let profileIndex = timelineList.index { (timeLineCellData) -> Bool in
            //timeLineCellData.userProfile?.id == userProfile?.id
            true
        }
        
        let cell = collectionView.cellForItem(at: IndexPath(row: profileIndex!, section:0)) as? TimelineCollectionViewCell
        cell?.collection.reloadItems(at: [index])
    }
    
   
    func reloadCollectionData() {
        
    }
    
}


extension HomeViewController:URLSessionDownloadDelegate{
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        for x in 0..<timelineList.count{
            for i in 0..<timelineList[x].picked_groups.count{
                for j in 0..<timelineList[x].picked_groups[i].picked_group.count{
                    if timelineList[x].picked_groups[i].picked_group[j].url == sourceURL.absoluteString{
                        let destinationURL = localFilePath(for: sourceURL)
                        print(destinationURL)
                        // 3
                        let fileManager = FileManager.default
                        try? fileManager.removeItem(at: destinationURL)
                        do {
                            try fileManager.copyItem(at: location, to: destinationURL)
                        } catch let error {
                            print("Could not copy file to disk: \(error.localizedDescription)")
                        }
                        timelineList[x].picked_groups[i].picked_group[j].downloadedUrl = destinationURL
                        DispatchQueue.main.async {
                            self.collectionView.reloadItems(at: [IndexPath(item: x, section: 0)] )
                        }
                    }
                }
            }
            
        }
        
        print("finished downloading")
        print(location)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print(Float(totalBytesWritten) / Float(totalBytesExpectedToWrite) * 100)
    }
    
}

