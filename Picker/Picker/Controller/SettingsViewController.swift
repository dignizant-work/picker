//
//  SettingsViewController.swift
//  Picker
//
//  Created by HARSHIT on 13/03/20.
//  Copyright © 2020 Omar basaleh. All rights reserved.
//

import UIKit
import QuickTableViewController
import OneSignal

class SettingsViewController: QuickTableViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Settings".localized()
        tableContents = [
            Section(title: " ", rows: [
                NavigationRow(text: "Profile".localized(), detailText: .value1(""), icon: .named("time"), action: { _ in
                    self.clk_Profile()
                }),
                NavigationRow(text: "Draft".localized(), detailText: .value1(""), icon: .named("time"), action: { _ in
                    self.clk_Draft()
                }),
            ]),
            Section(title: " ", rows: [
                TapActionRow(text: "Change password".localized(), action: { [weak self] in self?.showAlert($0) }),
                TapActionRow(text: "Blocked Users".localized(), action: { [weak self] in self?.showAlert($0) }),
                TapActionRow(text: "Privacy Policy".localized(), action: { [weak self] in self?.showAlert($0) }),
                TapActionRow(text: "Logout".localized(), action: { [weak self] in self?.showAlert($0) })
            ])
        ]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    func clk_Profile()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        profileViewController.isTabBarProfile = true
        profileViewController.isCurrentUser = true
        //profileViewController.tabBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "Account"), tag: 4)
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func clk_Draft() {
        self.navigationController?.pushViewController(DraftViewController(), animated: true)
    }
    
    func clk_PasswordChange()
    {
        self.navigationController?.pushViewController(ChangePasswordViewController(), animated: true)
    }
    
    func clk_BloackedUser()
    {
        self.navigationController?.pushViewController(BlockedUsersViewController(), animated: true)
    }
    
    func privacyPolicyVC() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let privacyVC = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        self.navigationController?.pushViewController(privacyVC, animated: true)
    }
    
    func clk_Logout()
    {
        if let token = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId{
            APIClient.logout(token: token, completion: { (error) in
                if let err = error{
                    print(err)
                    return
                }
                
                UserDefaults.standard.set(0, forKey: "userId")
                OneSignal.setSubscription(false)
                
                let loginVC = LoginViewController()
                loginVC.hero.isEnabled = true
                loginVC.hero.modalAnimationType = .fade
                self.tabBarController?.hero.replaceViewController(with: loginVC)
            })
        }
    }
    
    private func showAlert(_ sender: Row)
    {
        print(sender.text)
        
        switch sender.text
        {
        case "Change password".localized():
            clk_PasswordChange()
            break
        case "Blocked Users".localized():
            clk_BloackedUser()
            break
        case "Privacy Policy".localized():
            privacyPolicyVC()
            break
        case "Logout".localized():
            clk_Logout()
            break
        default:
            break
        }
    }
}

