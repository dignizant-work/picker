//
//  BlockedUsersViewController.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 5/21/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit

class BlockedUsersViewController: UIViewController {

    var blockedUsers = [UserShort]()
    private let blockedUserCellId = "blockedUserCellId"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Blocked Users".localized()
        setupViews()
        tableView.register(BlockedUserTableViewCell.self, forCellReuseIdentifier: blockedUserCellId)
        indicator.startAnimating()
        
        let userId = UserDefaults.standard.integer(forKey: "userId")
        APIClient.getBlockedUsers(user_id: userId) { (users, error) in
            
            DispatchQueue.main.async {
                self.indicator.stopAnimating()
            }
            
            if let err = error{
                print(err)
                return
            }
            
            if users.count > 0{
                self.tableView.separatorStyle = .singleLine
            }
            
            self.blockedUsers.append(contentsOf: users)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }
    
    let blueView:UIView = {
        let v = UIView()
        v.backgroundColor = AppColors.shared.blue
        return v
    }()
    
    let viewTitle:UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Raleway-Medium", size: 18)
        label.textColor = .white
        label.text = "Blocked Users".localized()
        return label
    }()
    
    lazy var tableView:UITableView = {
        let tv = UITableView()
        tv.separatorColor = AppColors.shared.superLightGray
        tv.separatorStyle = .none
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()
    
    let indicator:UIActivityIndicatorView = {
        var i = UIActivityIndicatorView()
        i.style = .gray
        return i
    }()
    
    let noNotificationLabel:UILabel = {
        let label = UILabel()
//        label.alpha = 0
        label.textAlignment = .center
        label.text = "No blocked users.".localized()
        label.textColor = AppColors.shared.gray
        label.font =  UIFont(name: "Raleway-Medium", size: 17)
        return label
    }()
    
    lazy var backBtn:UIButton = {
        let btn = UIButton()
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_right_36pt"), for: .normal)
        }else{
            btn.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left_36pt"), for: .normal)
        }
        btn.tintColor = .white
        btn.addTargetClosure(closure: { (btn) in
            self.dismiss(animated: true, completion: nil)
        })
        return btn
    }()
    
    
    
    func setupViews(){
//        view.addSubview(blueView)
//        blueView.snp.makeConstraints { (make) in
//            make.top.left.right.equalToSuperview()
//            if UIDevice().userInterfaceIdiom == .phone {
//                if  UIScreen.main.nativeBounds.height ==  2436{
//                    make.height.equalTo(88)
//                }else{
//                    make.height.equalTo(64)
//                }
//            }
//        }
//
//        blueView.addSubview(viewTitle)
//        viewTitle.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.bottom.equalToSuperview().inset(12)
//        }
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.bottom.right.equalToSuperview()
        }
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            //make.left.bottom.right.equalToSuperview()
        }
//
//        blueView.addSubview(backBtn)
//        backBtn.snp.makeConstraints { (make) in
//            make.leading.equalToSuperview().inset(8)
//            make.bottom.equalToSuperview().inset(8)
//            make.height.width.equalTo(35)
//        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(handleCancel))
    }

    @objc func handleCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension BlockedUsersViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if blockedUsers.count == 0 {
            tableView.backgroundView = self.noNotificationLabel
            return 0
        }
        tableView.backgroundView = nil
        return blockedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:blockedUserCellId , for: indexPath) as! BlockedUserTableViewCell
        cell.userProfile = blockedUsers[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController2") as! ProfileViewController
        vc.profile_user_id = blockedUsers[indexPath.row].id
        vc.hero.isEnabled = true
        
        if (Locale.preferredLanguages.first?.starts(with: "ar"))!{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .right), dismissing: .pull(direction: .left))
        }else{
            vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Unblock".localized()
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let userId = UserDefaults.standard.integer(forKey: "userId")
            
            APIClient.blockUser(user_id: userId, blocked_id: blockedUsers[indexPath.row].id) { (error) in
                if let err = error{
                    self.showMessageTop(body: err)
                    return
                }
            }
            
            self.blockedUsers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

}






