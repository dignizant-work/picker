//
//  AppDelegate.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/18/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import GooglePlaces
import OneSignal


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        GMSPlacesClient.provideAPIKey("AIzaSyCzKfICoaHeoGUtK7IWpqoe5fOXTmhxhsE")
        //AIzaSyB-kFSUsAeOQD8nDgyDhxuqYMpZP2t1HHo
        //AIzaSyB-kFSUsAeOQD8nDgyDhxuqYMpZP2t1HHo -- Omarbasaleh2 account
        //AIzaSyCzKfICoaHeoGUtK7IWpqoe5fOXTmhxhsE -- Vavisa account

        IQKeyboardManager.shared.enable = true
        
        // Sync hashed email if you have a login system or collect it.
        //   Will be used to reach the user at the most optimal time of day.
        // OneSignal.syncHashedEmail(userEmail)
        OneSignal.setSubscription(true)
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: oneSignalAppID,
                                        handleNotificationAction:  { result in
                                            
                                            // This block gets called when the user reacts to a notification received
                                            let payload: OSNotificationPayload = result!.notification.payload
                                            //Try to fetch the notification id (Deep link notification)
                                            if let additionalData = payload.additionalData as? [String: Any]{
                                                
                                                var notification = NotificationStruct()
                                                notification.id = additionalData["id"] as? Int ?? 0
                                                notification.user_id = additionalData["user_id"] as? Int ?? 0
                                                notification.username = additionalData["username"] as? String ?? ""
                                                notification.profile_image = additionalData["profile_image"] as? String ?? ""
                                                notification.is_follow = additionalData["is_follow"] as? Bool ?? false
                                                notification.is_accepted = additionalData["is_accepted"] as? Bool ?? false
                                                notification.message_ar = additionalData["message_ar"] as? String ?? ""
                                                notification.message_en = additionalData["message_en"] as? String ?? ""
                                                notification.type = additionalData["type"] as? String ?? ""
                                                notification.profile_image = additionalData["profile_image"] as? String ?? ""
                                                notification.comment_id = additionalData["comment_id"] as? Int ?? nil
                                                notification.picked_id = additionalData["picked_id"] as? Int ?? nil
                                                notification.picked_user_id = additionalData["picked_user_id"] as? Int ?? nil
                                                notification.picked_group_id = additionalData["picked_group_id"] as? Int ?? nil
                                                notification.reply_id = additionalData["reply_id"] as? Int ?? nil
                                                notification.is_seen = additionalData["is_seen"] as? Bool ?? false
                                                notification.readable_date = additionalData["readable_date"] as? String ?? ""
                                                
                                                
                                                
                                                
                                                self.window = UIWindow(frame: UIScreen.main.bounds)
                                                self.window?.makeKeyAndVisible()
                                                
                                                
                                                /**
                                                * For deep link notification go to camera view controller first
                                                * to avoid tab bar animation bug that happens when you go dirctly to Notification controller
                                                */
                                                
                                                if UserDefaults.standard.integer(forKey: "userId") > 0 {
                                                    let tabBarController = appTabBarController(deepLinkId: notification.id)
                                                    let nav = tabBarController.viewControllers![2] as! UINavigationController
                                                    let vc = nav.viewControllers.first as! CameraViewController
                                                    vc.deepLinkNotification = notification
                                                    self.window?.rootViewController = tabBarController
                                                }
                                               
                                                
                                                
                                                
                                            }
                                        },
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        // Add your AppDelegate as an obsserver
        OneSignal.add(self as OSSubscriptionObserver)
            
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        if UserDefaults.standard.integer(forKey: "userId") > 0 {
            window?.rootViewController = appTabBarController(deepLinkId: nil)
        }else{
            window?.rootViewController = LoginViewController()
        }
        
        
        //Status bar
        //UIApplication.shared.//                statusBarStyle = .lightContent
        
        //Customize navigation controller
        UINavigationBar.appearance().barTintColor = AppColors.shared.blue
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font: UIFont(name: "Raleway-Medium", size: 18)!]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name("reloadData"), object: nil)
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // This is used to get location in English
        UserDefaults.standard.removeObject(forKey: "AppleLanguages")
    }
    
}


extension AppDelegate:OSSubscriptionObserver{
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(String(describing: stateChanges))")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            UserDefaults.standard.set(playerId, forKey: "token")
            print("Current playerId \(playerId)")
        }
    }
    
}


