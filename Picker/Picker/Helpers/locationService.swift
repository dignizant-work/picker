//
//  locationService.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/29/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit
import CoreLocation

class LocationService{
    static let shared = LocationService()
    
    func getPlacemark(forLocation location: CLLocation, completionHandler: @escaping (CLPlacemark?, String?) -> ()) {
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in
            
            if let err = error {
                completionHandler(nil, err.localizedDescription)
            } else if let placemarkArray = placemarks {
                if let placemark = placemarkArray.first {
                    completionHandler(placemark, nil)
                } else {
                    completionHandler(nil, "Placemark was nil")
                }
            } else {
                completionHandler(nil, "Unknown error")
            }
        })
        
    }
}
