//
//  AppColors.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 10/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

final class AppColors{
    static let shared = AppColors()
    let yellow = UIColor(red: 255/255, green: 235/255, blue: 59/255, alpha: 1)
    let blue = UIColor(red: 93/255, green: 206/255, blue: 249/255, alpha: 1)
    let lightGray = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
    let gray = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)
    let darkGray = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
    let superLightGray = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
    let red = UIColor(red: 239/255, green: 83/255, blue: 80/255, alpha: 1)
}
