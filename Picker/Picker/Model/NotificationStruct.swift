//
//  Notification.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/19/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

struct NotificationStruct: Codable {
    var id: Int = 0
    var user_id: Int = 0
    var username: String = ""
    var name: String = ""
    var profile_image: String?
    var message_ar: String = ""
    var message_en: String = ""
    var is_follow: Bool = false
    var is_accepted: Bool = false
    var is_seen: Bool = false
    var is_tapped: Bool = false
    var comment_id: Int?
    var picked_id: Int?
    var picked_group_id: Int?
    var picked_user_id: Int?
    var reply_id: Int?
    var type: String = ""
    var readable_date: String = ""
}
