//
//  Timeline.swift
//  Picker
//
//  Created by Vavisa Mac on 6/23/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct Timeline: Codable {
    var user_id: Int = 0
    var name: String = ""
    var profile_image: String?
    var picked_groups:[PickedGroup] = []
}
