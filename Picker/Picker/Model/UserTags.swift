//
//  UserTags.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 6/20/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct UserTags: Codable {
    var user_tags: [String] = []
    var location_tags: [String] = []
}
