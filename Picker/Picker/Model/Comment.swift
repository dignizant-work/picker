//
//  Comment.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 2/18/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

class Comment2 {
    var id:String = ""
    var userId:String = ""
    var message:String = ""
    var replyCount: Int = 0
    var timestamp:[AnyHashable:Any] = [:]
    var timestampDouble:Double = 0.0
    var userProfile = UserProfile2()
    
    
    init(){}
    
    init(json:[String:Any]) {
        self.id = json["id"] as! String
        self.userId = json["userId"] as! String
        self.message = json["message"] as! String
        self.timestampDouble = json["timestamp"] as! Double
        self.replyCount = json["replyCount"] as! Int
    }
    
    func toDictionary() -> [String:Any]{
        
        return [ "id" : self.id,
                 "userId" : self.userId,
                 "message" : self.message,
                 "timestamp" : self.timestamp,
                 "replyCount" : self.replyCount
        ]
    }
}


