//
//  Comment.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 2/18/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct Comment: Codable {
    var id: Int = 0
    var user_id: Int = 0
    var message: String = ""
    var username:String = ""
    var name:String = ""
    var profile_image: String?
    var readable_time: String?
    var replies: [Reply] = []
}


struct Reply: Codable {
    var id: Int = 0
    var user_id: Int = 0
    var message: String = ""
    var username:String = ""
    var name:String = ""
    var readable_time: String?
    var profile_image: String = ""
}

