//
//  DraftPicked.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 4/26/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import UIKit
import RealmSwift

class DraftPicked: Object {
    @objc dynamic var id:String = ""
    @objc dynamic var user_id:String = ""
    @objc dynamic var is_video:Bool = false
    @objc dynamic var url:String = ""
    @objc dynamic var thumbUrl:String = ""
    @objc dynamic var location:String = ""
    @objc dynamic var caption:String = ""
    @objc dynamic var tag:String = ""
    @objc dynamic var tagId:String = ""
    @objc dynamic var comments_are_public:Bool = true
    var mentioned_usernames = List<String>()
}
