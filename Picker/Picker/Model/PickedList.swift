//
//  PickedGroupTagBased.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/15/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

struct PickedGroup{
    var userProfile:UserProfile?
    var pickedList:[Picked]?
}
