//
//  User.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 6/19/18.
//  Copyright © 2018 Omar basaleh. All rights reserved.
//

import Foundation

struct User:Codable{
    var id: Int = 0
    var name: String = ""
    var username: String = ""
    var bio: String?
    var profile_image: String?
    var cover_image: String?
    var email: String = ""
}
