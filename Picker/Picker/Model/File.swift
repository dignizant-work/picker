//
//  File.swift
//  Picker
//
//  Created by Omar mohammed on 7/28/19.
//  Copyright © 2019 Omar basaleh. All rights reserved.
//

import Foundation

struct PickedGroupResponse: Decodable{
    var data: [PickedGroup] = []
    var next_page_url: String?
}
