//
//  FollowListCellData.swift
//  Picker
//
//  Created by Vavisa - iMac 2 on 11/22/17.
//  Copyright © 2017 Omar basaleh. All rights reserved.
//

import UIKit

class FollowListCellData {
    var userProfile:UserProfile2?
    var isFollow:Bool?
    var isAccepted:Bool?
}
